# coding: utf-8

"""
    DLCM-Solution

    DLCM application wrapping all modules, All-In-One  # noqa: E501

    The version of the OpenAPI document: 2.1.1
    Contact: eresearch-opensource@unige.ch
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from dlcmapi_client.configuration import Configuration


class AipInfo(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'access': 'str',
        'access_currently_public': 'bool',
        'compliance_level': 'str',
        'contains_updated_metadata': 'bool',
        'current_access': 'str',
        'data_sensitivity': 'str',
        'description': 'str',
        'embargo': 'AipInfoEmbargo',
        'license_id': 'str',
        'metadata_version': 'str',
        'name': 'str',
        'organizational_unit_id': 'str',
        'status': 'str',
        'status_message': 'str'
    }

    attribute_map = {
        'access': 'access',
        'access_currently_public': 'accessCurrentlyPublic',
        'compliance_level': 'complianceLevel',
        'contains_updated_metadata': 'containsUpdatedMetadata',
        'current_access': 'currentAccess',
        'data_sensitivity': 'dataSensitivity',
        'description': 'description',
        'embargo': 'embargo',
        'license_id': 'licenseId',
        'metadata_version': 'metadataVersion',
        'name': 'name',
        'organizational_unit_id': 'organizationalUnitId',
        'status': 'status',
        'status_message': 'statusMessage'
    }

    def __init__(self, access=None, access_currently_public=None, compliance_level=None, contains_updated_metadata=None, current_access=None, data_sensitivity=None, description=None, embargo=None, license_id=None, metadata_version=None, name=None, organizational_unit_id=None, status=None, status_message=None, local_vars_configuration=None):  # noqa: E501
        """AipInfo - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._access = None
        self._access_currently_public = None
        self._compliance_level = None
        self._contains_updated_metadata = None
        self._current_access = None
        self._data_sensitivity = None
        self._description = None
        self._embargo = None
        self._license_id = None
        self._metadata_version = None
        self._name = None
        self._organizational_unit_id = None
        self._status = None
        self._status_message = None
        self.discriminator = None

        if access is not None:
            self.access = access
        if access_currently_public is not None:
            self.access_currently_public = access_currently_public
        if compliance_level is not None:
            self.compliance_level = compliance_level
        if contains_updated_metadata is not None:
            self.contains_updated_metadata = contains_updated_metadata
        if current_access is not None:
            self.current_access = current_access
        if data_sensitivity is not None:
            self.data_sensitivity = data_sensitivity
        if description is not None:
            self.description = description
        if embargo is not None:
            self.embargo = embargo
        if license_id is not None:
            self.license_id = license_id
        if metadata_version is not None:
            self.metadata_version = metadata_version
        if name is not None:
            self.name = name
        if organizational_unit_id is not None:
            self.organizational_unit_id = organizational_unit_id
        if status is not None:
            self.status = status
        if status_message is not None:
            self.status_message = status_message

    @property
    def access(self):
        """Gets the access of this AipInfo.  # noqa: E501

        The final access level of the package  # noqa: E501

        :return: The access of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._access

    @access.setter
    def access(self, access):
        """Sets the access of this AipInfo.

        The final access level of the package  # noqa: E501

        :param access: The access of this AipInfo.  # noqa: E501
        :type: str
        """

        self._access = access

    @property
    def access_currently_public(self):
        """Gets the access_currently_public of this AipInfo.  # noqa: E501

        A boolean indicating wether the current access level is PUBLIC  # noqa: E501

        :return: The access_currently_public of this AipInfo.  # noqa: E501
        :rtype: bool
        """
        return self._access_currently_public

    @access_currently_public.setter
    def access_currently_public(self, access_currently_public):
        """Sets the access_currently_public of this AipInfo.

        A boolean indicating wether the current access level is PUBLIC  # noqa: E501

        :param access_currently_public: The access_currently_public of this AipInfo.  # noqa: E501
        :type: bool
        """

        self._access_currently_public = access_currently_public

    @property
    def compliance_level(self):
        """Gets the compliance_level of this AipInfo.  # noqa: E501

        The compliance level of the package  # noqa: E501

        :return: The compliance_level of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._compliance_level

    @compliance_level.setter
    def compliance_level(self, compliance_level):
        """Sets the compliance_level of this AipInfo.

        The compliance level of the package  # noqa: E501

        :param compliance_level: The compliance_level of this AipInfo.  # noqa: E501
        :type: str
        """

        self._compliance_level = compliance_level

    @property
    def contains_updated_metadata(self):
        """Gets the contains_updated_metadata of this AipInfo.  # noqa: E501

        If the deposit contains updated metadata  # noqa: E501

        :return: The contains_updated_metadata of this AipInfo.  # noqa: E501
        :rtype: bool
        """
        return self._contains_updated_metadata

    @contains_updated_metadata.setter
    def contains_updated_metadata(self, contains_updated_metadata):
        """Sets the contains_updated_metadata of this AipInfo.

        If the deposit contains updated metadata  # noqa: E501

        :param contains_updated_metadata: The contains_updated_metadata of this AipInfo.  # noqa: E501
        :type: bool
        """

        self._contains_updated_metadata = contains_updated_metadata

    @property
    def current_access(self):
        """Gets the current_access of this AipInfo.  # noqa: E501

        The current access level, deducted from the final access level and an eventual embargo  # noqa: E501

        :return: The current_access of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._current_access

    @current_access.setter
    def current_access(self, current_access):
        """Sets the current_access of this AipInfo.

        The current access level, deducted from the final access level and an eventual embargo  # noqa: E501

        :param current_access: The current_access of this AipInfo.  # noqa: E501
        :type: str
        """

        self._current_access = current_access

    @property
    def data_sensitivity(self):
        """Gets the data_sensitivity of this AipInfo.  # noqa: E501

        The data sensitivity of the deposit  # noqa: E501

        :return: The data_sensitivity of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._data_sensitivity

    @data_sensitivity.setter
    def data_sensitivity(self, data_sensitivity):
        """Sets the data_sensitivity of this AipInfo.

        The data sensitivity of the deposit  # noqa: E501

        :param data_sensitivity: The data_sensitivity of this AipInfo.  # noqa: E501
        :type: str
        """

        self._data_sensitivity = data_sensitivity

    @property
    def description(self):
        """Gets the description of this AipInfo.  # noqa: E501

        The description of the package  # noqa: E501

        :return: The description of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this AipInfo.

        The description of the package  # noqa: E501

        :param description: The description of this AipInfo.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def embargo(self):
        """Gets the embargo of this AipInfo.  # noqa: E501


        :return: The embargo of this AipInfo.  # noqa: E501
        :rtype: AipInfoEmbargo
        """
        return self._embargo

    @embargo.setter
    def embargo(self, embargo):
        """Sets the embargo of this AipInfo.


        :param embargo: The embargo of this AipInfo.  # noqa: E501
        :type: AipInfoEmbargo
        """

        self._embargo = embargo

    @property
    def license_id(self):
        """Gets the license_id of this AipInfo.  # noqa: E501

        The license ID of the package  # noqa: E501

        :return: The license_id of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._license_id

    @license_id.setter
    def license_id(self, license_id):
        """Sets the license_id of this AipInfo.

        The license ID of the package  # noqa: E501

        :param license_id: The license_id of this AipInfo.  # noqa: E501
        :type: str
        """

        self._license_id = license_id

    @property
    def metadata_version(self):
        """Gets the metadata_version of this AipInfo.  # noqa: E501

        The metadata version of the deposit  # noqa: E501

        :return: The metadata_version of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._metadata_version

    @metadata_version.setter
    def metadata_version(self, metadata_version):
        """Sets the metadata_version of this AipInfo.

        The metadata version of the deposit  # noqa: E501

        :param metadata_version: The metadata_version of this AipInfo.  # noqa: E501
        :type: str
        """

        self._metadata_version = metadata_version

    @property
    def name(self):
        """Gets the name of this AipInfo.  # noqa: E501

        The name of the package  # noqa: E501

        :return: The name of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this AipInfo.

        The name of the package  # noqa: E501

        :param name: The name of this AipInfo.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def organizational_unit_id(self):
        """Gets the organizational_unit_id of this AipInfo.  # noqa: E501

        The organizational Unit ID of the information package  # noqa: E501

        :return: The organizational_unit_id of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._organizational_unit_id

    @organizational_unit_id.setter
    def organizational_unit_id(self, organizational_unit_id):
        """Sets the organizational_unit_id of this AipInfo.

        The organizational Unit ID of the information package  # noqa: E501

        :param organizational_unit_id: The organizational_unit_id of this AipInfo.  # noqa: E501
        :type: str
        """

        self._organizational_unit_id = organizational_unit_id

    @property
    def status(self):
        """Gets the status of this AipInfo.  # noqa: E501

        The status of the package  # noqa: E501

        :return: The status of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this AipInfo.

        The status of the package  # noqa: E501

        :param status: The status of this AipInfo.  # noqa: E501
        :type: str
        """

        self._status = status

    @property
    def status_message(self):
        """Gets the status_message of this AipInfo.  # noqa: E501

        The message related to the status  # noqa: E501

        :return: The status_message of this AipInfo.  # noqa: E501
        :rtype: str
        """
        return self._status_message

    @status_message.setter
    def status_message(self, status_message):
        """Sets the status_message of this AipInfo.

        The message related to the status  # noqa: E501

        :param status_message: The status_message of this AipInfo.  # noqa: E501
        :type: str
        """

        self._status_message = status_message

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, AipInfo):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, AipInfo):
            return True

        return self.to_dict() != other.to_dict()
