# coding: utf-8

"""
    DLCM-Solution

    DLCM application wrapping all modules, All-In-One  # noqa: E501

    The version of the OpenAPI document: 2.1.1
    Contact: eresearch-opensource@unige.ch
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from dlcmapi_client.configuration import Configuration


class Licens(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'links': 'object',
        'creation': 'ChangeInfo',
        'description': 'str',
        'domain_content': 'bool',
        'domain_data': 'bool',
        'domain_software': 'bool',
        'family': 'str',
        'is_generic': 'bool',
        'last_update': 'ChangeInfo',
        'maintainer': 'str',
        'od_conformance': 'str',
        'open_license_id': 'str',
        'osd_conformance': 'str',
        'res_id': 'str',
        'status': 'str',
        'title': 'str',
        'url': 'str'
    }

    attribute_map = {
        'links': '_links',
        'creation': 'creation',
        'description': 'description',
        'domain_content': 'domainContent',
        'domain_data': 'domainData',
        'domain_software': 'domainSoftware',
        'family': 'family',
        'is_generic': 'isGeneric',
        'last_update': 'lastUpdate',
        'maintainer': 'maintainer',
        'od_conformance': 'odConformance',
        'open_license_id': 'openLicenseId',
        'osd_conformance': 'osdConformance',
        'res_id': 'resId',
        'status': 'status',
        'title': 'title',
        'url': 'url'
    }

    def __init__(self, links=None, creation=None, description=None, domain_content=None, domain_data=None, domain_software=None, family=None, is_generic=None, last_update=None, maintainer=None, od_conformance=None, open_license_id=None, osd_conformance=None, res_id=None, status=None, title=None, url=None, local_vars_configuration=None):  # noqa: E501
        """Licens - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._links = None
        self._creation = None
        self._description = None
        self._domain_content = None
        self._domain_data = None
        self._domain_software = None
        self._family = None
        self._is_generic = None
        self._last_update = None
        self._maintainer = None
        self._od_conformance = None
        self._open_license_id = None
        self._osd_conformance = None
        self._res_id = None
        self._status = None
        self._title = None
        self._url = None
        self.discriminator = None

        if links is not None:
            self.links = links
        if creation is not None:
            self.creation = creation
        if description is not None:
            self.description = description
        if domain_content is not None:
            self.domain_content = domain_content
        if domain_data is not None:
            self.domain_data = domain_data
        if domain_software is not None:
            self.domain_software = domain_software
        if family is not None:
            self.family = family
        if is_generic is not None:
            self.is_generic = is_generic
        if last_update is not None:
            self.last_update = last_update
        if maintainer is not None:
            self.maintainer = maintainer
        if od_conformance is not None:
            self.od_conformance = od_conformance
        if open_license_id is not None:
            self.open_license_id = open_license_id
        if osd_conformance is not None:
            self.osd_conformance = osd_conformance
        if res_id is not None:
            self.res_id = res_id
        if status is not None:
            self.status = status
        if title is not None:
            self.title = title
        if url is not None:
            self.url = url

    @property
    def links(self):
        """Gets the links of this Licens.  # noqa: E501

        The _links_ list of the _licenses_ resource  # noqa: E501

        :return: The links of this Licens.  # noqa: E501
        :rtype: object
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this Licens.

        The _links_ list of the _licenses_ resource  # noqa: E501

        :param links: The links of this Licens.  # noqa: E501
        :type: object
        """

        self._links = links

    @property
    def creation(self):
        """Gets the creation of this Licens.  # noqa: E501


        :return: The creation of this Licens.  # noqa: E501
        :rtype: ChangeInfo
        """
        return self._creation

    @creation.setter
    def creation(self, creation):
        """Sets the creation of this Licens.


        :param creation: The creation of this Licens.  # noqa: E501
        :type: ChangeInfo
        """

        self._creation = creation

    @property
    def description(self):
        """Gets the description of this Licens.  # noqa: E501

        The description of the license  # noqa: E501

        :return: The description of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this Licens.

        The description of the license  # noqa: E501

        :param description: The description of this Licens.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def domain_content(self):
        """Gets the domain_content of this Licens.  # noqa: E501

        The content of the domain  # noqa: E501

        :return: The domain_content of this Licens.  # noqa: E501
        :rtype: bool
        """
        return self._domain_content

    @domain_content.setter
    def domain_content(self, domain_content):
        """Sets the domain_content of this Licens.

        The content of the domain  # noqa: E501

        :param domain_content: The domain_content of this Licens.  # noqa: E501
        :type: bool
        """

        self._domain_content = domain_content

    @property
    def domain_data(self):
        """Gets the domain_data of this Licens.  # noqa: E501

        The data domain  # noqa: E501

        :return: The domain_data of this Licens.  # noqa: E501
        :rtype: bool
        """
        return self._domain_data

    @domain_data.setter
    def domain_data(self, domain_data):
        """Sets the domain_data of this Licens.

        The data domain  # noqa: E501

        :param domain_data: The domain_data of this Licens.  # noqa: E501
        :type: bool
        """

        self._domain_data = domain_data

    @property
    def domain_software(self):
        """Gets the domain_software of this Licens.  # noqa: E501

        The domain software  # noqa: E501

        :return: The domain_software of this Licens.  # noqa: E501
        :rtype: bool
        """
        return self._domain_software

    @domain_software.setter
    def domain_software(self, domain_software):
        """Sets the domain_software of this Licens.

        The domain software  # noqa: E501

        :param domain_software: The domain_software of this Licens.  # noqa: E501
        :type: bool
        """

        self._domain_software = domain_software

    @property
    def family(self):
        """Gets the family of this Licens.  # noqa: E501

        The family license  # noqa: E501

        :return: The family of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._family

    @family.setter
    def family(self, family):
        """Sets the family of this Licens.

        The family license  # noqa: E501

        :param family: The family of this Licens.  # noqa: E501
        :type: str
        """

        self._family = family

    @property
    def is_generic(self):
        """Gets the is_generic of this Licens.  # noqa: E501

        Give if it's a generic license  # noqa: E501

        :return: The is_generic of this Licens.  # noqa: E501
        :rtype: bool
        """
        return self._is_generic

    @is_generic.setter
    def is_generic(self, is_generic):
        """Sets the is_generic of this Licens.

        Give if it's a generic license  # noqa: E501

        :param is_generic: The is_generic of this Licens.  # noqa: E501
        :type: bool
        """

        self._is_generic = is_generic

    @property
    def last_update(self):
        """Gets the last_update of this Licens.  # noqa: E501


        :return: The last_update of this Licens.  # noqa: E501
        :rtype: ChangeInfo
        """
        return self._last_update

    @last_update.setter
    def last_update(self, last_update):
        """Sets the last_update of this Licens.


        :param last_update: The last_update of this Licens.  # noqa: E501
        :type: ChangeInfo
        """

        self._last_update = last_update

    @property
    def maintainer(self):
        """Gets the maintainer of this Licens.  # noqa: E501

        The maintainer of the license  # noqa: E501

        :return: The maintainer of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._maintainer

    @maintainer.setter
    def maintainer(self, maintainer):
        """Sets the maintainer of this Licens.

        The maintainer of the license  # noqa: E501

        :param maintainer: The maintainer of this Licens.  # noqa: E501
        :type: str
        """

        self._maintainer = maintainer

    @property
    def od_conformance(self):
        """Gets the od_conformance of this Licens.  # noqa: E501

        The OD conformance status  # noqa: E501

        :return: The od_conformance of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._od_conformance

    @od_conformance.setter
    def od_conformance(self, od_conformance):
        """Sets the od_conformance of this Licens.

        The OD conformance status  # noqa: E501

        :param od_conformance: The od_conformance of this Licens.  # noqa: E501
        :type: str
        """
        allowed_values = ["approved", "", "not reviewed", "rejected"]  # noqa: E501
        if self.local_vars_configuration.client_side_validation and od_conformance not in allowed_values:  # noqa: E501
            raise ValueError(
                "Invalid value for `od_conformance` ({0}), must be one of {1}"  # noqa: E501
                .format(od_conformance, allowed_values)
            )

        self._od_conformance = od_conformance

    @property
    def open_license_id(self):
        """Gets the open_license_id of this Licens.  # noqa: E501

        The open license identifier  # noqa: E501

        :return: The open_license_id of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._open_license_id

    @open_license_id.setter
    def open_license_id(self, open_license_id):
        """Sets the open_license_id of this Licens.

        The open license identifier  # noqa: E501

        :param open_license_id: The open_license_id of this Licens.  # noqa: E501
        :type: str
        """

        self._open_license_id = open_license_id

    @property
    def osd_conformance(self):
        """Gets the osd_conformance of this Licens.  # noqa: E501

        The OSD conformance status  # noqa: E501

        :return: The osd_conformance of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._osd_conformance

    @osd_conformance.setter
    def osd_conformance(self, osd_conformance):
        """Sets the osd_conformance of this Licens.

        The OSD conformance status  # noqa: E501

        :param osd_conformance: The osd_conformance of this Licens.  # noqa: E501
        :type: str
        """
        allowed_values = ["approved", "", "not reviewed", "rejected"]  # noqa: E501
        if self.local_vars_configuration.client_side_validation and osd_conformance not in allowed_values:  # noqa: E501
            raise ValueError(
                "Invalid value for `osd_conformance` ({0}), must be one of {1}"  # noqa: E501
                .format(osd_conformance, allowed_values)
            )

        self._osd_conformance = osd_conformance

    @property
    def res_id(self):
        """Gets the res_id of this Licens.  # noqa: E501

        The _licenses_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]  # noqa: E501

        :return: The res_id of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._res_id

    @res_id.setter
    def res_id(self, res_id):
        """Sets the res_id of this Licens.

        The _licenses_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]  # noqa: E501

        :param res_id: The res_id of this Licens.  # noqa: E501
        :type: str
        """

        self._res_id = res_id

    @property
    def status(self):
        """Gets the status of this Licens.  # noqa: E501

        The current status of the license  # noqa: E501

        :return: The status of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this Licens.

        The current status of the license  # noqa: E501

        :param status: The status of this Licens.  # noqa: E501
        :type: str
        """
        allowed_values = ["active", "retired", "superseded"]  # noqa: E501
        if self.local_vars_configuration.client_side_validation and status not in allowed_values:  # noqa: E501
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def title(self):
        """Gets the title of this Licens.  # noqa: E501

        The title of the license  # noqa: E501

        :return: The title of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this Licens.

        The title of the license  # noqa: E501

        :param title: The title of this Licens.  # noqa: E501
        :type: str
        """

        self._title = title

    @property
    def url(self):
        """Gets the url of this Licens.  # noqa: E501

        The URL source of the license  # noqa: E501

        :return: The url of this Licens.  # noqa: E501
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """Sets the url of this Licens.

        The URL source of the license  # noqa: E501

        :param url: The url of this Licens.  # noqa: E501
        :type: str
        """

        self._url = url

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Licens):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Licens):
            return True

        return self.to_dict() != other.to_dict()
