# coding: utf-8

"""
    DLCM-Solution

    DLCM application wrapping all modules, All-In-One  # noqa: E501

    The version of the OpenAPI document: 2.1.1
    Contact: eresearch-opensource@unige.ch
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from dlcmapi_client.configuration import Configuration


class Role(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'links': 'object',
        'creation': 'ChangeInfo',
        'last_update': 'ChangeInfo',
        'level': 'float',
        'name': 'str',
        'res_id': 'str'
    }

    attribute_map = {
        'links': '_links',
        'creation': 'creation',
        'last_update': 'lastUpdate',
        'level': 'level',
        'name': 'name',
        'res_id': 'resId'
    }

    def __init__(self, links=None, creation=None, last_update=None, level=None, name=None, res_id=None, local_vars_configuration=None):  # noqa: E501
        """Role - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._links = None
        self._creation = None
        self._last_update = None
        self._level = None
        self._name = None
        self._res_id = None
        self.discriminator = None

        if links is not None:
            self.links = links
        if creation is not None:
            self.creation = creation
        if last_update is not None:
            self.last_update = last_update
        if level is not None:
            self.level = level
        if name is not None:
            self.name = name
        if res_id is not None:
            self.res_id = res_id

    @property
    def links(self):
        """Gets the links of this Role.  # noqa: E501

        The _links_ list of the _roles_ resource  # noqa: E501

        :return: The links of this Role.  # noqa: E501
        :rtype: object
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this Role.

        The _links_ list of the _roles_ resource  # noqa: E501

        :param links: The links of this Role.  # noqa: E501
        :type: object
        """

        self._links = links

    @property
    def creation(self):
        """Gets the creation of this Role.  # noqa: E501


        :return: The creation of this Role.  # noqa: E501
        :rtype: ChangeInfo
        """
        return self._creation

    @creation.setter
    def creation(self, creation):
        """Sets the creation of this Role.


        :param creation: The creation of this Role.  # noqa: E501
        :type: ChangeInfo
        """

        self._creation = creation

    @property
    def last_update(self):
        """Gets the last_update of this Role.  # noqa: E501


        :return: The last_update of this Role.  # noqa: E501
        :rtype: ChangeInfo
        """
        return self._last_update

    @last_update.setter
    def last_update(self, last_update):
        """Sets the last_update of this Role.


        :param last_update: The last_update of this Role.  # noqa: E501
        :type: ChangeInfo
        """

        self._last_update = last_update

    @property
    def level(self):
        """Gets the level of this Role.  # noqa: E501

        The level of the role  # noqa: E501

        :return: The level of this Role.  # noqa: E501
        :rtype: float
        """
        return self._level

    @level.setter
    def level(self, level):
        """Sets the level of this Role.

        The level of the role  # noqa: E501

        :param level: The level of this Role.  # noqa: E501
        :type: float
        """

        self._level = level

    @property
    def name(self):
        """Gets the name of this Role.  # noqa: E501

        The name of the role  # noqa: E501

        :return: The name of this Role.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Role.

        The name of the role  # noqa: E501

        :param name: The name of this Role.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def res_id(self):
        """Gets the res_id of this Role.  # noqa: E501

        The _roles_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]  # noqa: E501

        :return: The res_id of this Role.  # noqa: E501
        :rtype: str
        """
        return self._res_id

    @res_id.setter
    def res_id(self, res_id):
        """Sets the res_id of this Role.

        The _roles_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]  # noqa: E501

        :param res_id: The res_id of this Role.  # noqa: E501
        :type: str
        """

        self._res_id = res_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Role):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Role):
            return True

        return self.to_dict() != other.to_dict()
