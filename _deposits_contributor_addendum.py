#### "Custom" functions added for contributors management within a deposit.            

    def preingest_deposits_contributors_get(self, deposit_id, **kwargs):
        """preingest_deposits_contributors_get  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.preingest_deposits_get_by_id_with_http_info(res_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool: execute request asynchronously
        :param str deposit_id: Deposit Id (GUID) (required)
        :param _return_http_data_only: response data without head status code
                                       and headers
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: tuple(Deposit, status_code(int), headers(HTTPHeaderDict))
                 If the method is called asynchronously,
                 returns the request thread.
        """

        kwargs['_return_http_data_only'] = True
        local_var_params = locals()

        all_params = [
            'deposit_id'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method preingest_deposits_contributors_get" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']
        # verify the required parameter 'res_id' is set
        if self.api_client.client_side_validation and ('res_id' not in local_var_params or  # noqa: E501
                                                        local_var_params['res_id'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `res_id` when calling `preingest_deposits_get_by_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'deposit_id' in local_var_params:
            path_params['resId'] = local_var_params['deposit_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/hal+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['dlcm-auth']  # noqa: E501

        return self.api_client.call_api(
            '/preingest/deposits/{resId}/contributors', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Collection', 
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats)

    def preingest_deposits_contributors_post(self, deposit_id, contributors_list, **kwargs):  # noqa: E501
        """preingest_deposits_contributors_post  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.preingest_deposits_post_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool: execute request asynchronously
        :param str deposit_id str: identifier of the deposit in which contributors have to be added. (required)
        :param List[str] contributors_list: list of the GUID of the contributors to add to the deposit (required)
        
        :param _return_http_data_only: response data without head status code
                                       and headers
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: tuple(status_code(int), headers(HTTPHeaderDict))
                 If the method is called asynchronously,
                 returns the request thread.
        """

        kwargs['_return_http_data_only'] = True
        local_var_params = locals()

        all_params = [
            'deposit_id',
            'contributors_list'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method preingest_deposits_contributors_post" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'contributors_list' in local_var_params:
            body_params = local_var_params['contributors_list']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/hal+json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['dlcm-auth']  # noqa: E501

        return self.api_client.call_api(
            '/preingest/deposits/'+local_var_params['deposit_id']+'/contributors', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Collection',  # noqa: E501
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats)

    def preingest_deposits_contributors_delete_by_id(self, deposit_id, contributor_id, **kwargs):  # noqa: E501
        """preingest_deposits_contributors_delete_by_id  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.preingest_deposits_delete_by_id_with_http_info(res_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool: execute request asynchronously
        :param str deposit_id: Deposit Id (GUID) (required)
        :param str contributor_id: Deposit Id (GUID) (required)
        :param _return_http_data_only: response data without head status code
                                       and headers
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: tuple(Deposit, status_code(int), headers(HTTPHeaderDict))
                 If the method is called asynchronously,
                 returns the request thread.
        """

        kwargs['_return_http_data_only'] = True
        local_var_params = locals()

        all_params = [
            'deposit_id'
            'contributor_id'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method preingest_deposits_contributors_delete_by_id" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']
        # verify the required parameter 'res_id' is set
        if self.api_client.client_side_validation and ('res_id' not in local_var_params or  # noqa: E501
                                                        local_var_params['res_id'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `res_id` when calling `preingest_deposits_delete_by_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'deposit_id' in local_var_params:
            path_params['resId'] = local_var_params['deposit_id']  # noqa: E501
        
        if 'contributor_id' in local_var_params:
            path_params['contId'] = local_var_params['contributor_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/hal+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['dlcm-auth']  # noqa: E501

        return self.api_client.call_api(
            '/preingest/deposits/{resId}/contributors/{contId}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Contributor',  # noqa: E501
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats)
