# Module

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | **str** | The URL of _access_ module | [optional] 
**admin** | **str** | The URL of _admin_ module | [optional] 
**archival_storage** | **list[str]** | The URL list of _archival storage_ module | [optional] 
**authorization** | **str** | The URL of _authoriration_ module | [optional] 
**data_management** | **str** | The URL of _data management_ module | [optional] 
**index** | **str** | The URL of _index_ module | [optional] 
**ingest** | **str** | The URL of _ingest_ module | [optional] 
**oai_info** | **str** | The URL of _OAI-PMH_ module | [optional] 
**preingest** | **str** | The URL of _pre-ingest_ module | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


