# ArchiveType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _archive-types_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**master_archive_type** | **bool** | If the archive type is master type | [optional] 
**res_id** | **str** | The _archive-types_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**type_name** | **str** | The name of the archive type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


