# Page

Gives the total pages and the total available items of one resource
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_page** | **float** | The current page number | [optional] 
**size_page** | **float** | The number of item per page | [optional] 
**total_items** | **float** | The total number of items | [optional] 
**total_pages** | **float** | The total number of page | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


