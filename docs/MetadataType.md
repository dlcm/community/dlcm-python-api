# MetadataType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _metadata-types_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the metadata type | [optional] 
**full_name** | **str** | The name and the version of the metadata type | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**metadata_format** | **str** | The format of the metadata type | [optional] 
**metadata_schema** | **str** | The schema of the metadata type | [optional] 
**name** | **str** | The name of the metadata type | [optional] 
**res_id** | **str** | The _metadata-types_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**url** | **str** | The URL of the metadata type | [optional] 
**version** | **str** | The version of the metadata type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


