# dlcmapi_client.IngestApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ingest_get**](IngestApi.md#ingest_get) | **GET** /ingest | 
[**ingest_sip_delete_by_id**](IngestApi.md#ingest_sip_delete_by_id) | **DELETE** /ingest/sip/{resId} | 
[**ingest_sip_get**](IngestApi.md#ingest_sip_get) | **GET** /ingest/sip | 
[**ingest_sip_get_by_id**](IngestApi.md#ingest_sip_get_by_id) | **GET** /ingest/sip/{resId} | 
[**ingest_sip_patch_by_id**](IngestApi.md#ingest_sip_patch_by_id) | **PATCH** /ingest/sip/{resId} | 
[**ingest_sip_post**](IngestApi.md#ingest_sip_post) | **POST** /ingest/sip | 


# **ingest_get**
> Ingest ingest_get()



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.IngestApi(api_client)
    
    try:
        api_response = api_instance.ingest_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IngestApi->ingest_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Ingest**](Ingest.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ingest_sip_delete_by_id**
> Sip ingest_sip_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.IngestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.ingest_sip_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IngestApi->ingest_sip_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Sip**](Sip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ingest_sip_get**
> Collection ingest_sip_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.IngestApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.ingest_sip_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IngestApi->ingest_sip_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ingest_sip_get_by_id**
> Sip ingest_sip_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.IngestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.ingest_sip_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IngestApi->ingest_sip_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Sip**](Sip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ingest_sip_patch_by_id**
> Sip ingest_sip_patch_by_id(res_id, sip=sip)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.IngestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
sip = dlcmapi_client.Sip() # Sip |  (optional)

    try:
        api_response = api_instance.ingest_sip_patch_by_id(res_id, sip=sip)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IngestApi->ingest_sip_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **sip** | [**Sip**](Sip.md)|  | [optional] 

### Return type

[**Sip**](Sip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ingest_sip_post**
> Sip ingest_sip_post(sip=sip)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.IngestApi(api_client)
    sip = dlcmapi_client.Sip() # Sip |  (optional)

    try:
        api_response = api_instance.ingest_sip_post(sip=sip)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IngestApi->ingest_sip_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sip** | [**Sip**](Sip.md)|  | [optional] 

### Return type

[**Sip**](Sip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

