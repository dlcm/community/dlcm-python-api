# ChangeInfo

The change info
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**when** | **datetime** | The date of the executed event | [optional] 
**who** | **str** | The user who executed the event | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


