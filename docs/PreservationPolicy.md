# PreservationPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _preservation-policies_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**disposition_approval** | **bool** | Mandatory step to approve the AIP disposition | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the preservation policy | [optional] 
**res_id** | **str** | The _preservation-policies_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**retention** | **float** | The retention duration in days of the preservation policy | [optional] 
**smart_retention** | **str** | The retention duration in human-readable format of the preservation policy | [optional] 
**use_number** | **float** | Use number in organizational units of the preservation policy | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


