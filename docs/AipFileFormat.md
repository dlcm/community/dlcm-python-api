# AipFileFormat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type** | **str** | Content type of file | [optional] 
**details** | **str** | Format details of file | [optional] 
**format** | **str** | Format description of file | [optional] 
**md5** | **str** | MD5 checksum of file | [optional] 
**puid** | **str** | Format PUID of file | [optional] 
**version** | **str** | Format version of file | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


