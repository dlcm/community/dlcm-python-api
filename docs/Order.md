# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _orders_ resource | [optional] 
**aip_number** | **float** | The AIP number of the order query | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**dip_number** | **float** | The DIP number of the order query | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**metadata_version** | **str** | The metadata version of the deposit | [optional] 
**name** | **str** | The name of the order query | [optional] 
**public_order** | **bool** | True if the order is public, false otherwise | [optional] 
**query** | **str** | The query of the order query | [optional] 
**query_type** | **str** | The type of the order query | [optional] 
**res_id** | **str** | The _orders_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**status** | **str** | The status of the order query | [optional] 
**status_message** | **str** | The message related to the status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


