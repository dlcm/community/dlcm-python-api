# PreservationJob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _preservation-jobs_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**enable** | **bool** | If the job is enable | [optional] 
**execution_number** | **float** | Number of job executions | [optional] 
**job_recurrence** | **str** | Job recurrence | [optional] 
**job_type** | **str** | The job type of the preservation job | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the preservation job | [optional] 
**res_id** | **str** | The _preservation-jobs_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**scheduling** | [**PreservationJobScheduling**](PreservationJobScheduling.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


