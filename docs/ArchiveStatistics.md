# ArchiveStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _archive-statistics_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**download_number** | **float** | The download number of the archive | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**res_id** | **str** | The _archive-statistics_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**view_number** | **float** | The view number of the archive | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


