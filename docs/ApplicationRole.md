# ApplicationRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _application-roles_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**level** | **float** | The level of the role | [optional] 
**name** | **str** | The name of the application role | [optional] 
**res_id** | **str** | The _application-roles_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


