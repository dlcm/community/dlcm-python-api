# Metadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _metadata_ resource | [optional] 
**index** | **str** | The index name of indexed metadata | [optional] 
**metadata** | [**object**](.md) | The object of indexed metadata | [optional] 
**res_id** | **str** | The _metadata_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


