# SubmissionAgreement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _submission-agreements_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the submission agreement | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**res_id** | **str** | The _submission-agreements_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**title** | **str** | The title of the submission agreement | [optional] 
**version** | **str** | The version of the submission agreement | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


