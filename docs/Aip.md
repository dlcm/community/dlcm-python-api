# Aip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _aip_ resource | [optional] 
**archival_unit** | **bool** | if the AIP is an unit (AIU) or a collection (AIC) | [optional] 
**archive_container** | **str** | The container type of the AIP | [optional] 
**archive_file_number** | **float** | The total number of file in the AIP | [optional] 
**archive_id** | **str** | The storage URL of the AIP | [optional] 
**archive_size** | **float** | The size in bytes of the AIP | [optional] 
**checksum_check** | [**AipChecksumCheck**](AipChecksumCheck.md) |  | [optional] 
**checksums** | **list[str]** | The checksum list of the AIP | [optional] 
**collection_size** | **float** | Number of AIPs in collection | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**data_file_number** | **float** | Number of AIP data files | [optional] 
**disposition_approval** | **bool** | Mandatory step to approve the AIP disposition | [optional] 
**file_format** | [**AipFileFormat**](AipFileFormat.md) |  | [optional] 
**info** | [**AipInfo**](AipInfo.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**ready** | **bool** | If AIP package is ready | [optional] 
**res_id** | **str** | The _aip_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**retention** | **float** | The retention duration in days of the AIP | [optional] 
**retention_end** | **datetime** | The end of the AIP retention | [optional] 
**sip_ids** | **list[str]** | The source SIP IDs of the AIP | [optional] 
**smart_retention** | **str** | The retention duration in human-readable format of the AIP | [optional] 
**smart_size** | **str** | The size in human-readable format of the AIP | [optional] 
**smart_tombstone_size** | **str** | The size in human-readable format of the tombstone AIP | [optional] 
**tombstone_size** | **float** | The size in bytes of the tombstone AIP | [optional] 
**virus_check** | [**AipVirusCheck**](AipVirusCheck.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


