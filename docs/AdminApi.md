# dlcmapi_client.AdminApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**admin_application_roles_delete_by_id**](AdminApi.md#admin_application_roles_delete_by_id) | **DELETE** /admin/application-roles/{resId} | 
[**admin_application_roles_get**](AdminApi.md#admin_application_roles_get) | **GET** /admin/application-roles | 
[**admin_application_roles_get_by_id**](AdminApi.md#admin_application_roles_get_by_id) | **GET** /admin/application-roles/{resId} | 
[**admin_application_roles_patch_by_id**](AdminApi.md#admin_application_roles_patch_by_id) | **PATCH** /admin/application-roles/{resId} | 
[**admin_application_roles_post**](AdminApi.md#admin_application_roles_post) | **POST** /admin/application-roles | 
[**admin_archive_acl_delete_by_id**](AdminApi.md#admin_archive_acl_delete_by_id) | **DELETE** /admin/archive-acl/{resId} | 
[**admin_archive_acl_get**](AdminApi.md#admin_archive_acl_get) | **GET** /admin/archive-acl | 
[**admin_archive_acl_get_by_id**](AdminApi.md#admin_archive_acl_get_by_id) | **GET** /admin/archive-acl/{resId} | 
[**admin_archive_acl_patch_by_id**](AdminApi.md#admin_archive_acl_patch_by_id) | **PATCH** /admin/archive-acl/{resId} | 
[**admin_archive_acl_post**](AdminApi.md#admin_archive_acl_post) | **POST** /admin/archive-acl | 
[**admin_archive_ratings_delete_by_id**](AdminApi.md#admin_archive_ratings_delete_by_id) | **DELETE** /admin/archive-ratings/{resId} | 
[**admin_archive_ratings_get**](AdminApi.md#admin_archive_ratings_get) | **GET** /admin/archive-ratings | 
[**admin_archive_ratings_get_by_id**](AdminApi.md#admin_archive_ratings_get_by_id) | **GET** /admin/archive-ratings/{resId} | 
[**admin_archive_ratings_patch_by_id**](AdminApi.md#admin_archive_ratings_patch_by_id) | **PATCH** /admin/archive-ratings/{resId} | 
[**admin_archive_ratings_post**](AdminApi.md#admin_archive_ratings_post) | **POST** /admin/archive-ratings | 
[**admin_archive_statistics_get**](AdminApi.md#admin_archive_statistics_get) | **GET** /admin/archive-statistics | 
[**admin_archive_statistics_get_by_id**](AdminApi.md#admin_archive_statistics_get_by_id) | **GET** /admin/archive-statistics/{resId} | 
[**admin_archive_types_delete_by_id**](AdminApi.md#admin_archive_types_delete_by_id) | **DELETE** /admin/archive-types/{resId} | 
[**admin_archive_types_get**](AdminApi.md#admin_archive_types_get) | **GET** /admin/archive-types | 
[**admin_archive_types_get_by_id**](AdminApi.md#admin_archive_types_get_by_id) | **GET** /admin/archive-types/{resId} | 
[**admin_archive_types_patch_by_id**](AdminApi.md#admin_archive_types_patch_by_id) | **PATCH** /admin/archive-types/{resId} | 
[**admin_archive_types_post**](AdminApi.md#admin_archive_types_post) | **POST** /admin/archive-types | 
[**admin_authorized_organizational_units_get**](AdminApi.md#admin_authorized_organizational_units_get) | **GET** /admin/authorized-organizational-units | 
[**admin_dissemination_policies_delete_by_id**](AdminApi.md#admin_dissemination_policies_delete_by_id) | **DELETE** /admin/dissemination-policies/{resId} | 
[**admin_dissemination_policies_get**](AdminApi.md#admin_dissemination_policies_get) | **GET** /admin/dissemination-policies | 
[**admin_dissemination_policies_get_by_id**](AdminApi.md#admin_dissemination_policies_get_by_id) | **GET** /admin/dissemination-policies/{resId} | 
[**admin_dissemination_policies_patch_by_id**](AdminApi.md#admin_dissemination_policies_patch_by_id) | **PATCH** /admin/dissemination-policies/{resId} | 
[**admin_dissemination_policies_post**](AdminApi.md#admin_dissemination_policies_post) | **POST** /admin/dissemination-policies | 
[**admin_funding_agencies_delete_by_id**](AdminApi.md#admin_funding_agencies_delete_by_id) | **DELETE** /admin/funding-agencies/{resId} | 
[**admin_funding_agencies_get**](AdminApi.md#admin_funding_agencies_get) | **GET** /admin/funding-agencies | 
[**admin_funding_agencies_get_by_id**](AdminApi.md#admin_funding_agencies_get_by_id) | **GET** /admin/funding-agencies/{resId} | 
[**admin_funding_agencies_patch_by_id**](AdminApi.md#admin_funding_agencies_patch_by_id) | **PATCH** /admin/funding-agencies/{resId} | 
[**admin_funding_agencies_post**](AdminApi.md#admin_funding_agencies_post) | **POST** /admin/funding-agencies | 
[**admin_get**](AdminApi.md#admin_get) | **GET** /admin | 
[**admin_institutions_delete_by_id**](AdminApi.md#admin_institutions_delete_by_id) | **DELETE** /admin/institutions/{resId} | 
[**admin_institutions_get**](AdminApi.md#admin_institutions_get) | **GET** /admin/institutions | 
[**admin_institutions_get_by_id**](AdminApi.md#admin_institutions_get_by_id) | **GET** /admin/institutions/{resId} | 
[**admin_institutions_patch_by_id**](AdminApi.md#admin_institutions_patch_by_id) | **PATCH** /admin/institutions/{resId} | 
[**admin_institutions_post**](AdminApi.md#admin_institutions_post) | **POST** /admin/institutions | 
[**admin_languages_delete_by_id**](AdminApi.md#admin_languages_delete_by_id) | **DELETE** /admin/languages/{resId} | 
[**admin_languages_get**](AdminApi.md#admin_languages_get) | **GET** /admin/languages | 
[**admin_languages_get_by_id**](AdminApi.md#admin_languages_get_by_id) | **GET** /admin/languages/{resId} | 
[**admin_languages_patch_by_id**](AdminApi.md#admin_languages_patch_by_id) | **PATCH** /admin/languages/{resId} | 
[**admin_languages_post**](AdminApi.md#admin_languages_post) | **POST** /admin/languages | 
[**admin_licenses_delete_by_id**](AdminApi.md#admin_licenses_delete_by_id) | **DELETE** /admin/licenses/{resId} | 
[**admin_licenses_get**](AdminApi.md#admin_licenses_get) | **GET** /admin/licenses | 
[**admin_licenses_get_by_id**](AdminApi.md#admin_licenses_get_by_id) | **GET** /admin/licenses/{resId} | 
[**admin_licenses_patch_by_id**](AdminApi.md#admin_licenses_patch_by_id) | **PATCH** /admin/licenses/{resId} | 
[**admin_licenses_post**](AdminApi.md#admin_licenses_post) | **POST** /admin/licenses | 
[**admin_metadata_types_delete_by_id**](AdminApi.md#admin_metadata_types_delete_by_id) | **DELETE** /admin/metadata-types/{resId} | 
[**admin_metadata_types_get**](AdminApi.md#admin_metadata_types_get) | **GET** /admin/metadata-types | 
[**admin_metadata_types_get_by_id**](AdminApi.md#admin_metadata_types_get_by_id) | **GET** /admin/metadata-types/{resId} | 
[**admin_metadata_types_patch_by_id**](AdminApi.md#admin_metadata_types_patch_by_id) | **PATCH** /admin/metadata-types/{resId} | 
[**admin_metadata_types_post**](AdminApi.md#admin_metadata_types_post) | **POST** /admin/metadata-types | 
[**admin_notifications_delete_by_id**](AdminApi.md#admin_notifications_delete_by_id) | **DELETE** /admin/notifications/{resId} | 
[**admin_notifications_get**](AdminApi.md#admin_notifications_get) | **GET** /admin/notifications | 
[**admin_notifications_get_by_id**](AdminApi.md#admin_notifications_get_by_id) | **GET** /admin/notifications/{resId} | 
[**admin_notifications_patch_by_id**](AdminApi.md#admin_notifications_patch_by_id) | **PATCH** /admin/notifications/{resId} | 
[**admin_notifications_post**](AdminApi.md#admin_notifications_post) | **POST** /admin/notifications | 
[**admin_organizational_units_delete_by_id**](AdminApi.md#admin_organizational_units_delete_by_id) | **DELETE** /admin/organizational-units/{resId} | 
[**admin_organizational_units_get**](AdminApi.md#admin_organizational_units_get) | **GET** /admin/organizational-units | 
[**admin_organizational_units_get_by_id**](AdminApi.md#admin_organizational_units_get_by_id) | **GET** /admin/organizational-units/{resId} | 
[**admin_organizational_units_patch_by_id**](AdminApi.md#admin_organizational_units_patch_by_id) | **PATCH** /admin/organizational-units/{resId} | 
[**admin_organizational_units_post**](AdminApi.md#admin_organizational_units_post) | **POST** /admin/organizational-units | 
[**admin_people_delete_by_id**](AdminApi.md#admin_people_delete_by_id) | **DELETE** /admin/people/{resId} | 
[**admin_people_get**](AdminApi.md#admin_people_get) | **GET** /admin/people | 
[**admin_people_get_by_id**](AdminApi.md#admin_people_get_by_id) | **GET** /admin/people/{resId} | 
[**admin_people_patch_by_id**](AdminApi.md#admin_people_patch_by_id) | **PATCH** /admin/people/{resId} | 
[**admin_people_post**](AdminApi.md#admin_people_post) | **POST** /admin/people | 
[**admin_preservation_policies_delete_by_id**](AdminApi.md#admin_preservation_policies_delete_by_id) | **DELETE** /admin/preservation-policies/{resId} | 
[**admin_preservation_policies_get**](AdminApi.md#admin_preservation_policies_get) | **GET** /admin/preservation-policies | 
[**admin_preservation_policies_get_by_id**](AdminApi.md#admin_preservation_policies_get_by_id) | **GET** /admin/preservation-policies/{resId} | 
[**admin_preservation_policies_patch_by_id**](AdminApi.md#admin_preservation_policies_patch_by_id) | **PATCH** /admin/preservation-policies/{resId} | 
[**admin_preservation_policies_post**](AdminApi.md#admin_preservation_policies_post) | **POST** /admin/preservation-policies | 
[**admin_rating_types_delete_by_id**](AdminApi.md#admin_rating_types_delete_by_id) | **DELETE** /admin/rating-types/{resId} | 
[**admin_rating_types_get**](AdminApi.md#admin_rating_types_get) | **GET** /admin/rating-types | 
[**admin_rating_types_get_by_id**](AdminApi.md#admin_rating_types_get_by_id) | **GET** /admin/rating-types/{resId} | 
[**admin_rating_types_patch_by_id**](AdminApi.md#admin_rating_types_patch_by_id) | **PATCH** /admin/rating-types/{resId} | 
[**admin_rating_types_post**](AdminApi.md#admin_rating_types_post) | **POST** /admin/rating-types | 
[**admin_research_domains_delete_by_id**](AdminApi.md#admin_research_domains_delete_by_id) | **DELETE** /admin/research-domains/{resId} | 
[**admin_research_domains_get**](AdminApi.md#admin_research_domains_get) | **GET** /admin/research-domains | 
[**admin_research_domains_get_by_id**](AdminApi.md#admin_research_domains_get_by_id) | **GET** /admin/research-domains/{resId} | 
[**admin_research_domains_patch_by_id**](AdminApi.md#admin_research_domains_patch_by_id) | **PATCH** /admin/research-domains/{resId} | 
[**admin_research_domains_post**](AdminApi.md#admin_research_domains_post) | **POST** /admin/research-domains | 
[**admin_roles_delete_by_id**](AdminApi.md#admin_roles_delete_by_id) | **DELETE** /admin/roles/{resId} | 
[**admin_roles_get**](AdminApi.md#admin_roles_get) | **GET** /admin/roles | 
[**admin_roles_get_by_id**](AdminApi.md#admin_roles_get_by_id) | **GET** /admin/roles/{resId} | 
[**admin_roles_patch_by_id**](AdminApi.md#admin_roles_patch_by_id) | **PATCH** /admin/roles/{resId} | 
[**admin_roles_post**](AdminApi.md#admin_roles_post) | **POST** /admin/roles | 
[**admin_scheduled_tasks_delete_by_id**](AdminApi.md#admin_scheduled_tasks_delete_by_id) | **DELETE** /admin/scheduled-tasks/{resId} | 
[**admin_scheduled_tasks_get**](AdminApi.md#admin_scheduled_tasks_get) | **GET** /admin/scheduled-tasks | 
[**admin_scheduled_tasks_get_by_id**](AdminApi.md#admin_scheduled_tasks_get_by_id) | **GET** /admin/scheduled-tasks/{resId} | 
[**admin_scheduled_tasks_patch_by_id**](AdminApi.md#admin_scheduled_tasks_patch_by_id) | **PATCH** /admin/scheduled-tasks/{resId} | 
[**admin_scheduled_tasks_post**](AdminApi.md#admin_scheduled_tasks_post) | **POST** /admin/scheduled-tasks | 
[**admin_submission_agreements_delete_by_id**](AdminApi.md#admin_submission_agreements_delete_by_id) | **DELETE** /admin/submission-agreements/{resId} | 
[**admin_submission_agreements_get**](AdminApi.md#admin_submission_agreements_get) | **GET** /admin/submission-agreements | 
[**admin_submission_agreements_get_by_id**](AdminApi.md#admin_submission_agreements_get_by_id) | **GET** /admin/submission-agreements/{resId} | 
[**admin_submission_agreements_patch_by_id**](AdminApi.md#admin_submission_agreements_patch_by_id) | **PATCH** /admin/submission-agreements/{resId} | 
[**admin_submission_agreements_post**](AdminApi.md#admin_submission_agreements_post) | **POST** /admin/submission-agreements | 
[**admin_submission_policies_delete_by_id**](AdminApi.md#admin_submission_policies_delete_by_id) | **DELETE** /admin/submission-policies/{resId} | 
[**admin_submission_policies_get**](AdminApi.md#admin_submission_policies_get) | **GET** /admin/submission-policies | 
[**admin_submission_policies_get_by_id**](AdminApi.md#admin_submission_policies_get_by_id) | **GET** /admin/submission-policies/{resId} | 
[**admin_submission_policies_patch_by_id**](AdminApi.md#admin_submission_policies_patch_by_id) | **PATCH** /admin/submission-policies/{resId} | 
[**admin_submission_policies_post**](AdminApi.md#admin_submission_policies_post) | **POST** /admin/submission-policies | 
[**admin_system_properties_get**](AdminApi.md#admin_system_properties_get) | **GET** /admin/system-properties | 
[**admin_users_delete_by_id**](AdminApi.md#admin_users_delete_by_id) | **DELETE** /admin/users/{resId} | 
[**admin_users_get**](AdminApi.md#admin_users_get) | **GET** /admin/users | 
[**admin_users_get_by_id**](AdminApi.md#admin_users_get_by_id) | **GET** /admin/users/{resId} | 
[**admin_users_patch_by_id**](AdminApi.md#admin_users_patch_by_id) | **PATCH** /admin/users/{resId} | 
[**admin_users_post**](AdminApi.md#admin_users_post) | **POST** /admin/users | 


# **admin_application_roles_delete_by_id**
> ApplicationRole admin_application_roles_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_application_roles_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_application_roles_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ApplicationRole**](ApplicationRole.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_application_roles_get**
> Collection admin_application_roles_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_application_roles_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_application_roles_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_application_roles_get_by_id**
> ApplicationRole admin_application_roles_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_application_roles_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_application_roles_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ApplicationRole**](ApplicationRole.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_application_roles_patch_by_id**
> ApplicationRole admin_application_roles_patch_by_id(res_id, application_role=application_role)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
application_role = dlcmapi_client.ApplicationRole() # ApplicationRole |  (optional)

    try:
        api_response = api_instance.admin_application_roles_patch_by_id(res_id, application_role=application_role)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_application_roles_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **application_role** | [**ApplicationRole**](ApplicationRole.md)|  | [optional] 

### Return type

[**ApplicationRole**](ApplicationRole.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_application_roles_post**
> ApplicationRole admin_application_roles_post(application_role=application_role)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    application_role = dlcmapi_client.ApplicationRole() # ApplicationRole |  (optional)

    try:
        api_response = api_instance.admin_application_roles_post(application_role=application_role)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_application_roles_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **application_role** | [**ApplicationRole**](ApplicationRole.md)|  | [optional] 

### Return type

[**ApplicationRole**](ApplicationRole.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_acl_delete_by_id**
> ArchiveAcl admin_archive_acl_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_acl_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_acl_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveAcl**](ArchiveAcl.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_acl_get**
> Collection admin_archive_acl_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_archive_acl_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_acl_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_acl_get_by_id**
> ArchiveAcl admin_archive_acl_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_acl_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_acl_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveAcl**](ArchiveAcl.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_acl_patch_by_id**
> ArchiveAcl admin_archive_acl_patch_by_id(res_id, archive_acl=archive_acl)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
archive_acl = dlcmapi_client.ArchiveAcl() # ArchiveAcl |  (optional)

    try:
        api_response = api_instance.admin_archive_acl_patch_by_id(res_id, archive_acl=archive_acl)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_acl_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **archive_acl** | [**ArchiveAcl**](ArchiveAcl.md)|  | [optional] 

### Return type

[**ArchiveAcl**](ArchiveAcl.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_acl_post**
> ArchiveAcl admin_archive_acl_post(archive_acl=archive_acl)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    archive_acl = dlcmapi_client.ArchiveAcl() # ArchiveAcl |  (optional)

    try:
        api_response = api_instance.admin_archive_acl_post(archive_acl=archive_acl)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_acl_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive_acl** | [**ArchiveAcl**](ArchiveAcl.md)|  | [optional] 

### Return type

[**ArchiveAcl**](ArchiveAcl.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_ratings_delete_by_id**
> ArchiveRating admin_archive_ratings_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_ratings_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_ratings_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveRating**](ArchiveRating.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_ratings_get**
> Collection admin_archive_ratings_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_archive_ratings_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_ratings_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_ratings_get_by_id**
> ArchiveRating admin_archive_ratings_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_ratings_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_ratings_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveRating**](ArchiveRating.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_ratings_patch_by_id**
> ArchiveRating admin_archive_ratings_patch_by_id(res_id, archive_rating=archive_rating)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
archive_rating = dlcmapi_client.ArchiveRating() # ArchiveRating |  (optional)

    try:
        api_response = api_instance.admin_archive_ratings_patch_by_id(res_id, archive_rating=archive_rating)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_ratings_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **archive_rating** | [**ArchiveRating**](ArchiveRating.md)|  | [optional] 

### Return type

[**ArchiveRating**](ArchiveRating.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_ratings_post**
> ArchiveRating admin_archive_ratings_post(archive_rating=archive_rating)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    archive_rating = dlcmapi_client.ArchiveRating() # ArchiveRating |  (optional)

    try:
        api_response = api_instance.admin_archive_ratings_post(archive_rating=archive_rating)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_ratings_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive_rating** | [**ArchiveRating**](ArchiveRating.md)|  | [optional] 

### Return type

[**ArchiveRating**](ArchiveRating.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_statistics_get**
> Collection admin_archive_statistics_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_archive_statistics_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_statistics_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_statistics_get_by_id**
> ArchiveStatistics admin_archive_statistics_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_statistics_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_statistics_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveStatistics**](ArchiveStatistics.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_types_delete_by_id**
> ArchiveType admin_archive_types_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_types_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_types_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveType**](ArchiveType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_types_get**
> Collection admin_archive_types_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_archive_types_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_types_get_by_id**
> ArchiveType admin_archive_types_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_archive_types_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_types_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ArchiveType**](ArchiveType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_types_patch_by_id**
> ArchiveType admin_archive_types_patch_by_id(res_id, archive_type=archive_type)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
archive_type = dlcmapi_client.ArchiveType() # ArchiveType |  (optional)

    try:
        api_response = api_instance.admin_archive_types_patch_by_id(res_id, archive_type=archive_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_types_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **archive_type** | [**ArchiveType**](ArchiveType.md)|  | [optional] 

### Return type

[**ArchiveType**](ArchiveType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_archive_types_post**
> ArchiveType admin_archive_types_post(archive_type=archive_type)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    archive_type = dlcmapi_client.ArchiveType() # ArchiveType |  (optional)

    try:
        api_response = api_instance.admin_archive_types_post(archive_type=archive_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_archive_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive_type** | [**ArchiveType**](ArchiveType.md)|  | [optional] 

### Return type

[**ArchiveType**](ArchiveType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_authorized_organizational_units_get**
> Collection admin_authorized_organizational_units_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_authorized_organizational_units_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_authorized_organizational_units_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_dissemination_policies_delete_by_id**
> DisseminationPolicy admin_dissemination_policies_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_dissemination_policies_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_dissemination_policies_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**DisseminationPolicy**](DisseminationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_dissemination_policies_get**
> Collection admin_dissemination_policies_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_dissemination_policies_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_dissemination_policies_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_dissemination_policies_get_by_id**
> DisseminationPolicy admin_dissemination_policies_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_dissemination_policies_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_dissemination_policies_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**DisseminationPolicy**](DisseminationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_dissemination_policies_patch_by_id**
> DisseminationPolicy admin_dissemination_policies_patch_by_id(res_id, dissemination_policy=dissemination_policy)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
dissemination_policy = dlcmapi_client.DisseminationPolicy() # DisseminationPolicy |  (optional)

    try:
        api_response = api_instance.admin_dissemination_policies_patch_by_id(res_id, dissemination_policy=dissemination_policy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_dissemination_policies_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **dissemination_policy** | [**DisseminationPolicy**](DisseminationPolicy.md)|  | [optional] 

### Return type

[**DisseminationPolicy**](DisseminationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_dissemination_policies_post**
> DisseminationPolicy admin_dissemination_policies_post(dissemination_policy=dissemination_policy)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    dissemination_policy = dlcmapi_client.DisseminationPolicy() # DisseminationPolicy |  (optional)

    try:
        api_response = api_instance.admin_dissemination_policies_post(dissemination_policy=dissemination_policy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_dissemination_policies_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dissemination_policy** | [**DisseminationPolicy**](DisseminationPolicy.md)|  | [optional] 

### Return type

[**DisseminationPolicy**](DisseminationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_funding_agencies_delete_by_id**
> FundingAgency admin_funding_agencies_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_funding_agencies_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_funding_agencies_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**FundingAgency**](FundingAgency.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_funding_agencies_get**
> Collection admin_funding_agencies_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_funding_agencies_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_funding_agencies_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_funding_agencies_get_by_id**
> FundingAgency admin_funding_agencies_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_funding_agencies_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_funding_agencies_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**FundingAgency**](FundingAgency.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_funding_agencies_patch_by_id**
> FundingAgency admin_funding_agencies_patch_by_id(res_id, funding_agency=funding_agency)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
funding_agency = dlcmapi_client.FundingAgency() # FundingAgency |  (optional)

    try:
        api_response = api_instance.admin_funding_agencies_patch_by_id(res_id, funding_agency=funding_agency)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_funding_agencies_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **funding_agency** | [**FundingAgency**](FundingAgency.md)|  | [optional] 

### Return type

[**FundingAgency**](FundingAgency.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_funding_agencies_post**
> FundingAgency admin_funding_agencies_post(funding_agency=funding_agency)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    funding_agency = dlcmapi_client.FundingAgency() # FundingAgency |  (optional)

    try:
        api_response = api_instance.admin_funding_agencies_post(funding_agency=funding_agency)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_funding_agencies_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **funding_agency** | [**FundingAgency**](FundingAgency.md)|  | [optional] 

### Return type

[**FundingAgency**](FundingAgency.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_get**
> Admin admin_get()



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    
    try:
        api_response = api_instance.admin_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Admin**](Admin.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_institutions_delete_by_id**
> Institution admin_institutions_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_institutions_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_institutions_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Institution**](Institution.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_institutions_get**
> Collection admin_institutions_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_institutions_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_institutions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_institutions_get_by_id**
> Institution admin_institutions_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_institutions_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_institutions_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Institution**](Institution.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_institutions_patch_by_id**
> Institution admin_institutions_patch_by_id(res_id, institution=institution)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
institution = dlcmapi_client.Institution() # Institution |  (optional)

    try:
        api_response = api_instance.admin_institutions_patch_by_id(res_id, institution=institution)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_institutions_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **institution** | [**Institution**](Institution.md)|  | [optional] 

### Return type

[**Institution**](Institution.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_institutions_post**
> Institution admin_institutions_post(institution=institution)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    institution = dlcmapi_client.Institution() # Institution |  (optional)

    try:
        api_response = api_instance.admin_institutions_post(institution=institution)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_institutions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **institution** | [**Institution**](Institution.md)|  | [optional] 

### Return type

[**Institution**](Institution.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_languages_delete_by_id**
> Language admin_languages_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_languages_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_languages_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Language**](Language.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_languages_get**
> Collection admin_languages_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_languages_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_languages_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_languages_get_by_id**
> Language admin_languages_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_languages_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_languages_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Language**](Language.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_languages_patch_by_id**
> Language admin_languages_patch_by_id(res_id, language=language)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
language = dlcmapi_client.Language() # Language |  (optional)

    try:
        api_response = api_instance.admin_languages_patch_by_id(res_id, language=language)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_languages_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **language** | [**Language**](Language.md)|  | [optional] 

### Return type

[**Language**](Language.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_languages_post**
> Language admin_languages_post(language=language)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    language = dlcmapi_client.Language() # Language |  (optional)

    try:
        api_response = api_instance.admin_languages_post(language=language)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_languages_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language** | [**Language**](Language.md)|  | [optional] 

### Return type

[**Language**](Language.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_licenses_delete_by_id**
> Licens admin_licenses_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_licenses_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_licenses_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Licens**](Licens.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_licenses_get**
> Collection admin_licenses_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_licenses_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_licenses_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_licenses_get_by_id**
> Licens admin_licenses_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_licenses_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_licenses_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Licens**](Licens.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_licenses_patch_by_id**
> Licens admin_licenses_patch_by_id(res_id, licens=licens)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
licens = dlcmapi_client.Licens() # Licens |  (optional)

    try:
        api_response = api_instance.admin_licenses_patch_by_id(res_id, licens=licens)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_licenses_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **licens** | [**Licens**](Licens.md)|  | [optional] 

### Return type

[**Licens**](Licens.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_licenses_post**
> Licens admin_licenses_post(licens=licens)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    licens = dlcmapi_client.Licens() # Licens |  (optional)

    try:
        api_response = api_instance.admin_licenses_post(licens=licens)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_licenses_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licens** | [**Licens**](Licens.md)|  | [optional] 

### Return type

[**Licens**](Licens.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_metadata_types_delete_by_id**
> MetadataType admin_metadata_types_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_metadata_types_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_metadata_types_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**MetadataType**](MetadataType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_metadata_types_get**
> Collection admin_metadata_types_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_metadata_types_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_metadata_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_metadata_types_get_by_id**
> MetadataType admin_metadata_types_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_metadata_types_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_metadata_types_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**MetadataType**](MetadataType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_metadata_types_patch_by_id**
> MetadataType admin_metadata_types_patch_by_id(res_id, metadata_type=metadata_type)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
metadata_type = dlcmapi_client.MetadataType() # MetadataType |  (optional)

    try:
        api_response = api_instance.admin_metadata_types_patch_by_id(res_id, metadata_type=metadata_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_metadata_types_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **metadata_type** | [**MetadataType**](MetadataType.md)|  | [optional] 

### Return type

[**MetadataType**](MetadataType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_metadata_types_post**
> MetadataType admin_metadata_types_post(metadata_type=metadata_type)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    metadata_type = dlcmapi_client.MetadataType() # MetadataType |  (optional)

    try:
        api_response = api_instance.admin_metadata_types_post(metadata_type=metadata_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_metadata_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **metadata_type** | [**MetadataType**](MetadataType.md)|  | [optional] 

### Return type

[**MetadataType**](MetadataType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_notifications_delete_by_id**
> Notification admin_notifications_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_notifications_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_notifications_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Notification**](Notification.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_notifications_get**
> Collection admin_notifications_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_notifications_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_notifications_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_notifications_get_by_id**
> Notification admin_notifications_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_notifications_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_notifications_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Notification**](Notification.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_notifications_patch_by_id**
> Notification admin_notifications_patch_by_id(res_id, notification=notification)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
notification = dlcmapi_client.Notification() # Notification |  (optional)

    try:
        api_response = api_instance.admin_notifications_patch_by_id(res_id, notification=notification)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_notifications_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **notification** | [**Notification**](Notification.md)|  | [optional] 

### Return type

[**Notification**](Notification.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_notifications_post**
> Notification admin_notifications_post(notification=notification)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    notification = dlcmapi_client.Notification() # Notification |  (optional)

    try:
        api_response = api_instance.admin_notifications_post(notification=notification)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_notifications_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notification** | [**Notification**](Notification.md)|  | [optional] 

### Return type

[**Notification**](Notification.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_organizational_units_delete_by_id**
> OrganizationalUnit admin_organizational_units_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_organizational_units_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_organizational_units_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**OrganizationalUnit**](OrganizationalUnit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_organizational_units_get**
> Collection admin_organizational_units_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_organizational_units_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_organizational_units_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_organizational_units_get_by_id**
> OrganizationalUnit admin_organizational_units_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_organizational_units_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_organizational_units_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**OrganizationalUnit**](OrganizationalUnit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_organizational_units_patch_by_id**
> OrganizationalUnit admin_organizational_units_patch_by_id(res_id, organizational_unit=organizational_unit)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
organizational_unit = dlcmapi_client.OrganizationalUnit() # OrganizationalUnit |  (optional)

    try:
        api_response = api_instance.admin_organizational_units_patch_by_id(res_id, organizational_unit=organizational_unit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_organizational_units_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **organizational_unit** | [**OrganizationalUnit**](OrganizationalUnit.md)|  | [optional] 

### Return type

[**OrganizationalUnit**](OrganizationalUnit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_organizational_units_post**
> OrganizationalUnit admin_organizational_units_post(organizational_unit=organizational_unit)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    organizational_unit = dlcmapi_client.OrganizationalUnit() # OrganizationalUnit |  (optional)

    try:
        api_response = api_instance.admin_organizational_units_post(organizational_unit=organizational_unit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_organizational_units_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizational_unit** | [**OrganizationalUnit**](OrganizationalUnit.md)|  | [optional] 

### Return type

[**OrganizationalUnit**](OrganizationalUnit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_people_delete_by_id**
> Person admin_people_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_people_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_people_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Person**](Person.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_people_get**
> Collection admin_people_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_people_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_people_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_people_get_by_id**
> Person admin_people_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_people_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_people_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Person**](Person.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_people_patch_by_id**
> Person admin_people_patch_by_id(res_id, person=person)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
person = dlcmapi_client.Person() # Person |  (optional)

    try:
        api_response = api_instance.admin_people_patch_by_id(res_id, person=person)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_people_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **person** | [**Person**](Person.md)|  | [optional] 

### Return type

[**Person**](Person.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_people_post**
> Person admin_people_post(person=person)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    person = dlcmapi_client.Person() # Person |  (optional)

    try:
        api_response = api_instance.admin_people_post(person=person)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_people_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **person** | [**Person**](Person.md)|  | [optional] 

### Return type

[**Person**](Person.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_preservation_policies_delete_by_id**
> PreservationPolicy admin_preservation_policies_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_preservation_policies_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_preservation_policies_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**PreservationPolicy**](PreservationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_preservation_policies_get**
> Collection admin_preservation_policies_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_preservation_policies_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_preservation_policies_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_preservation_policies_get_by_id**
> PreservationPolicy admin_preservation_policies_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_preservation_policies_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_preservation_policies_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**PreservationPolicy**](PreservationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_preservation_policies_patch_by_id**
> PreservationPolicy admin_preservation_policies_patch_by_id(res_id, preservation_policy=preservation_policy)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
preservation_policy = dlcmapi_client.PreservationPolicy() # PreservationPolicy |  (optional)

    try:
        api_response = api_instance.admin_preservation_policies_patch_by_id(res_id, preservation_policy=preservation_policy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_preservation_policies_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **preservation_policy** | [**PreservationPolicy**](PreservationPolicy.md)|  | [optional] 

### Return type

[**PreservationPolicy**](PreservationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_preservation_policies_post**
> PreservationPolicy admin_preservation_policies_post(preservation_policy=preservation_policy)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    preservation_policy = dlcmapi_client.PreservationPolicy() # PreservationPolicy |  (optional)

    try:
        api_response = api_instance.admin_preservation_policies_post(preservation_policy=preservation_policy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_preservation_policies_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **preservation_policy** | [**PreservationPolicy**](PreservationPolicy.md)|  | [optional] 

### Return type

[**PreservationPolicy**](PreservationPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_rating_types_delete_by_id**
> RatingType admin_rating_types_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_rating_types_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_rating_types_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**RatingType**](RatingType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_rating_types_get**
> Collection admin_rating_types_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_rating_types_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_rating_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_rating_types_get_by_id**
> RatingType admin_rating_types_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_rating_types_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_rating_types_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**RatingType**](RatingType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_rating_types_patch_by_id**
> RatingType admin_rating_types_patch_by_id(res_id, rating_type=rating_type)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
rating_type = dlcmapi_client.RatingType() # RatingType |  (optional)

    try:
        api_response = api_instance.admin_rating_types_patch_by_id(res_id, rating_type=rating_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_rating_types_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **rating_type** | [**RatingType**](RatingType.md)|  | [optional] 

### Return type

[**RatingType**](RatingType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_rating_types_post**
> RatingType admin_rating_types_post(rating_type=rating_type)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    rating_type = dlcmapi_client.RatingType() # RatingType |  (optional)

    try:
        api_response = api_instance.admin_rating_types_post(rating_type=rating_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_rating_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rating_type** | [**RatingType**](RatingType.md)|  | [optional] 

### Return type

[**RatingType**](RatingType.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_research_domains_delete_by_id**
> ResearchDomain admin_research_domains_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_research_domains_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_research_domains_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ResearchDomain**](ResearchDomain.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_research_domains_get**
> Collection admin_research_domains_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_research_domains_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_research_domains_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_research_domains_get_by_id**
> ResearchDomain admin_research_domains_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_research_domains_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_research_domains_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ResearchDomain**](ResearchDomain.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_research_domains_patch_by_id**
> ResearchDomain admin_research_domains_patch_by_id(res_id, research_domain=research_domain)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
research_domain = dlcmapi_client.ResearchDomain() # ResearchDomain |  (optional)

    try:
        api_response = api_instance.admin_research_domains_patch_by_id(res_id, research_domain=research_domain)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_research_domains_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **research_domain** | [**ResearchDomain**](ResearchDomain.md)|  | [optional] 

### Return type

[**ResearchDomain**](ResearchDomain.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_research_domains_post**
> ResearchDomain admin_research_domains_post(research_domain=research_domain)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    research_domain = dlcmapi_client.ResearchDomain() # ResearchDomain |  (optional)

    try:
        api_response = api_instance.admin_research_domains_post(research_domain=research_domain)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_research_domains_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **research_domain** | [**ResearchDomain**](ResearchDomain.md)|  | [optional] 

### Return type

[**ResearchDomain**](ResearchDomain.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_roles_delete_by_id**
> Role admin_roles_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_roles_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_roles_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Role**](Role.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_roles_get**
> Collection admin_roles_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_roles_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_roles_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_roles_get_by_id**
> Role admin_roles_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_roles_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_roles_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Role**](Role.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_roles_patch_by_id**
> Role admin_roles_patch_by_id(res_id, role=role)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
role = dlcmapi_client.Role() # Role |  (optional)

    try:
        api_response = api_instance.admin_roles_patch_by_id(res_id, role=role)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_roles_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **role** | [**Role**](Role.md)|  | [optional] 

### Return type

[**Role**](Role.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_roles_post**
> Role admin_roles_post(role=role)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    role = dlcmapi_client.Role() # Role |  (optional)

    try:
        api_response = api_instance.admin_roles_post(role=role)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_roles_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role** | [**Role**](Role.md)|  | [optional] 

### Return type

[**Role**](Role.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_scheduled_tasks_delete_by_id**
> ScheduledTask admin_scheduled_tasks_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_scheduled_tasks_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_scheduled_tasks_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ScheduledTask**](ScheduledTask.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_scheduled_tasks_get**
> Collection admin_scheduled_tasks_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_scheduled_tasks_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_scheduled_tasks_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_scheduled_tasks_get_by_id**
> ScheduledTask admin_scheduled_tasks_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_scheduled_tasks_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_scheduled_tasks_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**ScheduledTask**](ScheduledTask.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_scheduled_tasks_patch_by_id**
> ScheduledTask admin_scheduled_tasks_patch_by_id(res_id, scheduled_task=scheduled_task)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
scheduled_task = dlcmapi_client.ScheduledTask() # ScheduledTask |  (optional)

    try:
        api_response = api_instance.admin_scheduled_tasks_patch_by_id(res_id, scheduled_task=scheduled_task)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_scheduled_tasks_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **scheduled_task** | [**ScheduledTask**](ScheduledTask.md)|  | [optional] 

### Return type

[**ScheduledTask**](ScheduledTask.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_scheduled_tasks_post**
> ScheduledTask admin_scheduled_tasks_post(scheduled_task=scheduled_task)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    scheduled_task = dlcmapi_client.ScheduledTask() # ScheduledTask |  (optional)

    try:
        api_response = api_instance.admin_scheduled_tasks_post(scheduled_task=scheduled_task)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_scheduled_tasks_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduled_task** | [**ScheduledTask**](ScheduledTask.md)|  | [optional] 

### Return type

[**ScheduledTask**](ScheduledTask.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_agreements_delete_by_id**
> SubmissionAgreement admin_submission_agreements_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_submission_agreements_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_agreements_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**SubmissionAgreement**](SubmissionAgreement.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_agreements_get**
> Collection admin_submission_agreements_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_submission_agreements_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_agreements_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_agreements_get_by_id**
> SubmissionAgreement admin_submission_agreements_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_submission_agreements_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_agreements_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**SubmissionAgreement**](SubmissionAgreement.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_agreements_patch_by_id**
> SubmissionAgreement admin_submission_agreements_patch_by_id(res_id, submission_agreement=submission_agreement)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
submission_agreement = dlcmapi_client.SubmissionAgreement() # SubmissionAgreement |  (optional)

    try:
        api_response = api_instance.admin_submission_agreements_patch_by_id(res_id, submission_agreement=submission_agreement)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_agreements_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **submission_agreement** | [**SubmissionAgreement**](SubmissionAgreement.md)|  | [optional] 

### Return type

[**SubmissionAgreement**](SubmissionAgreement.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_agreements_post**
> SubmissionAgreement admin_submission_agreements_post(submission_agreement=submission_agreement)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    submission_agreement = dlcmapi_client.SubmissionAgreement() # SubmissionAgreement |  (optional)

    try:
        api_response = api_instance.admin_submission_agreements_post(submission_agreement=submission_agreement)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_agreements_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **submission_agreement** | [**SubmissionAgreement**](SubmissionAgreement.md)|  | [optional] 

### Return type

[**SubmissionAgreement**](SubmissionAgreement.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_policies_delete_by_id**
> SubmissionPolicy admin_submission_policies_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_submission_policies_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_policies_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**SubmissionPolicy**](SubmissionPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_policies_get**
> Collection admin_submission_policies_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_submission_policies_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_policies_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_policies_get_by_id**
> SubmissionPolicy admin_submission_policies_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_submission_policies_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_policies_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**SubmissionPolicy**](SubmissionPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_policies_patch_by_id**
> SubmissionPolicy admin_submission_policies_patch_by_id(res_id, submission_policy=submission_policy)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
submission_policy = dlcmapi_client.SubmissionPolicy() # SubmissionPolicy |  (optional)

    try:
        api_response = api_instance.admin_submission_policies_patch_by_id(res_id, submission_policy=submission_policy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_policies_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **submission_policy** | [**SubmissionPolicy**](SubmissionPolicy.md)|  | [optional] 

### Return type

[**SubmissionPolicy**](SubmissionPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_submission_policies_post**
> SubmissionPolicy admin_submission_policies_post(submission_policy=submission_policy)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    submission_policy = dlcmapi_client.SubmissionPolicy() # SubmissionPolicy |  (optional)

    try:
        api_response = api_instance.admin_submission_policies_post(submission_policy=submission_policy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_submission_policies_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **submission_policy** | [**SubmissionPolicy**](SubmissionPolicy.md)|  | [optional] 

### Return type

[**SubmissionPolicy**](SubmissionPolicy.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_system_properties_get**
> SystemProperty admin_system_properties_get()



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    
    try:
        api_response = api_instance.admin_system_properties_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_system_properties_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SystemProperty**](SystemProperty.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_users_delete_by_id**
> User admin_users_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_users_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_users_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**User**](User.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_users_get**
> Collection admin_users_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.admin_users_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_users_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_users_get_by_id**
> User admin_users_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.admin_users_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_users_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**User**](User.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_users_patch_by_id**
> User admin_users_patch_by_id(res_id, user=user)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
user = dlcmapi_client.User() # User |  (optional)

    try:
        api_response = api_instance.admin_users_patch_by_id(res_id, user=user)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_users_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **user** | [**User**](User.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **admin_users_post**
> User admin_users_post(user=user)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AdminApi(api_client)
    user = dlcmapi_client.User() # User |  (optional)

    try:
        api_response = api_instance.admin_users_post(user=user)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AdminApi->admin_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

