# Language

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _languages_ resource | [optional] 
**code** | **str** | The code of the language | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**res_id** | **str** | The _languages_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


