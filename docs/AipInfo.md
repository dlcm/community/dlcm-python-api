# AipInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | **str** | The final access level of the package | [optional] 
**access_currently_public** | **bool** | A boolean indicating wether the current access level is PUBLIC | [optional] 
**compliance_level** | **str** | The compliance level of the package | [optional] 
**contains_updated_metadata** | **bool** | If the deposit contains updated metadata | [optional] 
**current_access** | **str** | The current access level, deducted from the final access level and an eventual embargo | [optional] 
**data_sensitivity** | **str** | The data sensitivity of the deposit | [optional] 
**description** | **str** | The description of the package | [optional] 
**embargo** | [**AipInfoEmbargo**](AipInfoEmbargo.md) |  | [optional] 
**license_id** | **str** | The license ID of the package | [optional] 
**metadata_version** | **str** | The metadata version of the deposit | [optional] 
**name** | **str** | The name of the package | [optional] 
**organizational_unit_id** | **str** | The organizational Unit ID of the information package | [optional] 
**status** | **str** | The status of the package | [optional] 
**status_message** | **str** | The message related to the status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


