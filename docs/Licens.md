# Licens

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _licenses_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the license | [optional] 
**domain_content** | **bool** | The content of the domain | [optional] 
**domain_data** | **bool** | The data domain | [optional] 
**domain_software** | **bool** | The domain software | [optional] 
**family** | **str** | The family license | [optional] 
**is_generic** | **bool** | Give if it&#39;s a generic license | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**maintainer** | **str** | The maintainer of the license | [optional] 
**od_conformance** | **str** | The OD conformance status | [optional] 
**open_license_id** | **str** | The open license identifier | [optional] 
**osd_conformance** | **str** | The OSD conformance status | [optional] 
**res_id** | **str** | The _licenses_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**status** | **str** | The current status of the license | [optional] 
**title** | **str** | The title of the license | [optional] 
**url** | **str** | The URL source of the license | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


