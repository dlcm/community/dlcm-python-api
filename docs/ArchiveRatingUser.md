# ArchiveRatingUser

The user of the archive rating
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**res_id** | **str** | The user ID of the archive rating | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


