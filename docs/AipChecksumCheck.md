# AipChecksumCheck

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**check_date** | **str** | Date of last checksum check execution | [optional] 
**checking_succeed** | **bool** | Result of last checksum check execution | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


