# Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the application | [optional] 
**name** | **str** | The name of the application | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


