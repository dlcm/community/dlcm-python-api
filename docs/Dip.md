# Dip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _dip_ resource | [optional] 
**aip_ids** | **list[str]** | The source AIP IDs of the DIP | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**info** | [**AipInfo**](AipInfo.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**ready** | **bool** | If DIP package is ready | [optional] 
**res_id** | **str** | The _dip_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


