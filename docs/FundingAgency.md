# FundingAgency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _funding-agencies_ resource | [optional] 
**acronym** | **str** | The acronym of the funding agency | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the funding agency | [optional] 
**identifiers** | [**object**](.md) | The external identifier list of the funding agency | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the funding agency | [optional] 
**res_id** | **str** | The _funding-agencies_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**ror_id** | **str** | The ROR identifier of the funding agency | [optional] 
**url** | **str** | The URL of the funding agency | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


