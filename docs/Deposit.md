# Deposit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _deposits_ resource | [optional] 
**access** | **str** | The access level of the deposit | [optional] 
**additional_fields_form_id** | **str** | The additional fields form ID used to generate the additional fields values | [optional] 
**additional_fields_values** | **str** | Some eventual additional fields values generated through an additional fields form | [optional] 
**agreement** | **bool** | The submission agreement of the deposit | [optional] 
**archive_type_id** | **str** | The archive type ID of the deposit | [optional] 
**collection_begin** | **datetime** | The beginning of the deposit collection | [optional] 
**collection_end** | **datetime** | The end of the deposit collection | [optional] 
**collection_size** | **float** | Size of AIP collection | [optional] 
**contains_updated_metadata** | **bool** | If the deposit contains updated metadata | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**data_file_number** | **float** | Number of deposit data files | [optional] 
**data_sensitivity** | **str** | The data sensitivity of the deposit | [optional] 
**description** | **str** | The description of the deposit | [optional] 
**doi** | **str** | The Digital Object Identifier (DOI) of the deposit | [optional] 
**embargo** | [**AipInfoEmbargo**](AipInfoEmbargo.md) |  | [optional] 
**has_embargo** | **bool** | If the deposit has an embargo | [optional] 
**is_identical_to** | **str** | Indicates that Archive A is identical to B, used to register two separate instances of the same resource | [optional] 
**is_obsoleted_by** | **str** | Indicates A is replaced by B | [optional] 
**is_referenced_by** | **str** | Indicates A is used as a source of information by B | [optional] 
**keywords** | **list[str]** | List of deposit keywords | [optional] 
**language_id** | **str** | The language ID of the deposit | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**license_id** | **str** | The license ID of the deposit | [optional] 
**metadata_version** | **str** | The metadata version of the deposit | [optional] 
**organizational_unit_id** | **str** | The organizational unit ID of the deposit | [optional] 
**preparation_id** | **str** | The preparation ID of the deposit | [optional] 
**preservation_policy_id** | **str** | The preservation policy ID of the deposit | [optional] 
**publication_date** | **date** | The publication date of the deposit | [optional] 
**res_id** | **str** | The _deposits_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**sip_id** | **str** | The generated SIP DLCM ID of the deposit | [optional] 
**status** | **str** | The status of the deposit | [optional] 
**status_message** | **str** | The message related to the status | [optional] 
**submission_agreement_id** | **str** | The submission agreement of the deposit | [optional] 
**submission_policy_id** | **str** | The submission policy ID of the deposit | [optional] 
**title** | **str** | The title of the deposit | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


