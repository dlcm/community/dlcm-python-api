# Notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _notifications_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**emitter** | [**object**](.md) | The emitter of the notification (User) | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**message** | **str** | The message sent by the notification | [optional] 
**notification_status** | **str** | The status of the notification | [optional] 
**notification_type** | [**NotificationNotificationType**](NotificationNotificationType.md) |  | [optional] 
**notified_org_unit** | [**NotificationNotifiedOrgUnit**](NotificationNotifiedOrgUnit.md) |  | [optional] 
**object_id** | **str** | The target object ID of the notification | [optional] 
**recipient** | [**NotificationRecipient**](NotificationRecipient.md) |  | [optional] 
**res_id** | **str** | The _notifications_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**status_message** | **str** | The message related to the status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


