# NotificationNotificationType

The type of the notification
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**res_id** | **str** | The type of the notification | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


