# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _users_ resource | [optional] 
**application_role** | [**object**](.md) | The application role details of the user | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**email** | **str** | The email of the user | [optional] 
**enabled** | **bool** | If the user is enable | [optional] 
**external_uid** | **str** | The external UID of the user | [optional] 
**first_name** | **str** | The first name of the user | [optional] 
**full_name** | **str** | The full name of the user | [optional] 
**home_organization** | **str** | The home organization of the user | [optional] 
**last_name** | **str** | The last name of the user | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**res_id** | **str** | The _users_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


