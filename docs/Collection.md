# Collection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**object**](.md) | List the collection of the resources | [optional] 
**links** | [**object**](.md) | All links available on the resource | [optional] 
**page** | [**Page**](.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


