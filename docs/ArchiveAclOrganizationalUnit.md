# ArchiveAclOrganizationalUnit

The organizational unit of the archive ACL
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**res_id** | **str** | The organizational unit ID of the archive ACL | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


