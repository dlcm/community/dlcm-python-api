# NotificationRecipient

The recipient of the notification (Person)
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**res_id** | **str** | The recipient of the notification (Person id) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


