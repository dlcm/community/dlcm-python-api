# ArchiveRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _archive-ratings_ resource | [optional] 
**archive_id** | **str** | The archive ID of the archive rating | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**grade** | **float** | The grade of the archive rating | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**rating_type** | [**ArchiveRatingRatingType**](ArchiveRatingRatingType.md) |  | [optional] 
**res_id** | **str** | The _archive-ratings_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**user** | [**ArchiveRatingUser**](ArchiveRatingUser.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


