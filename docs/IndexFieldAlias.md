# IndexFieldAlias

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _index-field-aliases_ resource | [optional] 
**alias** | **str** | The name that will be exposed to clients (as a field or a facet) | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**facet** | **bool** | Should a facet be created for this field ? default to false | [optional] 
**facet_default_visible_values** | **float** | Number of facet values displayed by default in the user interface | [optional] 
**facet_limit** | **float** | Maximum number of facets values to return if the IndexFieldAlias is a facet | [optional] 
**facet_min_count** | **float** | Minimum number of occurrences of a facet value to be returned in facet results if the IndexFieldAlias is a facet | [optional] 
**facet_order** | **float** | Determines the position of a facet in the user interface | [optional] 
**field** | **str** | The field path in the index | [optional] 
**index_name** | **str** | The name of the ElasticSearch index the field is for | [optional] 
**labels** | **list[str]** | Alias labels for each supported languages | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**res_id** | **str** | The _index-field-aliases_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**system** | **bool** | Is used by system | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


