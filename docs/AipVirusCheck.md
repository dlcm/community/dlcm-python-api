# AipVirusCheck

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**check_date** | **str** | Date of virus check execution | [optional] 
**details** | **str** | Result detail of virus check | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


