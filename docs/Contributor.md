# Contributor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _contributors_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**deposit_number** | **float** | The deposit number of the contributor | [optional] 
**first_name** | **str** | The first name of the contributor | [optional] 
**full_name** | **str** | The full name of the contributor | [optional] 
**last_name** | **str** | The last name of the contributor | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**orcid** | **str** | The ORCID of the contributor | [optional] 
**res_id** | **str** | The _contributors_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**verified_orcid** | **bool** | if the contributor ORCID is verified | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


