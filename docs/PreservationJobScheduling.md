# PreservationJobScheduling

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hour** | **float** | The hour to schedule a daily/weekly/monthly/yearly job | [optional] 
**month** | **float** | The month to schedule a yearly job | [optional] 
**month_day** | **float** | The day of the month to schedule a monthly/yearly job | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


