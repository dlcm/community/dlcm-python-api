# Sip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _sip_ resource | [optional] 
**aip_id** | **str** | The generated AIP DLCM ID of the SIP | [optional] 
**collection_size** | **float** | Size of AIP collection | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**data_file_number** | **float** | Number of SIP data files | [optional] 
**deposit_id** | **str** | The source deposit ID of the SIP | [optional] 
**disposition_approval** | **bool** | Mandatory step to approve the AIP disposition | [optional] 
**info** | [**AipInfo**](AipInfo.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**organizational_unit_id** | **str** | The organizational unit ID of the SIP | [optional] 
**ready** | **bool** | If the SIP package is ready | [optional] 
**res_id** | **str** | The _sip_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**retention** | **float** | The retention duration in days of the AIP | [optional] 
**smart_retention** | **str** | The retention duration in human-readable format of the AIP | [optional] 
**submission_policy_id** | **str** | The submission policy ID of the SIP | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


