# ResearchDomain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _research-domains_ resource | [optional] 
**code** | **str** | The code of the research domain | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**labels** | **list[str]** | Name labels for each supported languages | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the research domain | [optional] 
**res_id** | **str** | The _research-domains_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**source** | **str** | The source of the research domain | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


