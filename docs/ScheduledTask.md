# ScheduledTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _scheduled-tasks_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**cron_expression** | **str** | The CRON expression of the scheduled task | [optional] 
**enabled** | **bool** | If the task is enable | [optional] 
**last_execution_end** | **datetime** | The last execution end of the scheduled task | [optional] 
**last_execution_start** | **datetime** | The last execution start of the scheduled task | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the scheduled task | [optional] 
**next_execution** | **datetime** | The next execution date of the scheduled task | [optional] 
**res_id** | **str** | The _scheduled-tasks_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**task_type** | **str** | The type of the scheduled task | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


