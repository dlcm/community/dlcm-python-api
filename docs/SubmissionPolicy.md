# SubmissionPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _submission-policies_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the submission policy | [optional] 
**res_id** | **str** | The _submission-policies_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**submission_approval** | **bool** | Mandatory step to approve a deposit | [optional] 
**time_to_keep** | **float** | Time to keep completed deposit before purge | [optional] 
**use_number** | **float** | Use number in organizational units of the submission policy | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


