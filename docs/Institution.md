# Institution

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _institutions_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the institution | [optional] 
**email_suffixes** | **list[str]** | List of email suffixes | [optional] 
**identifiers** | [**object**](.md) | The external identifier list of the institution | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the institution | [optional] 
**res_id** | **str** | The _institutions_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**ror_id** | **str** | The ROR identifier of the institution | [optional] 
**url** | **str** | The URL of the institution website | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


