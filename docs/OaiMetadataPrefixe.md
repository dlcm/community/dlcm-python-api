# OaiMetadataPrefixe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _oai-metadata-prefixes_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the OAI metadata prefix | [optional] 
**enabled** | **bool** | If the OAI metadata prefix is enable | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**metadata_schema** | **str** | The XML schema of the OAI metadata prefix | [optional] 
**metadata_xml_transformation** | **str** | The XML transformation from reference metadata of the OAI metadata prefix | [optional] 
**name** | **str** | The name of the OAI metadata prefix | [optional] 
**prefix** | **str** | The specification of the OAI metadata prefix | [optional] 
**reference** | **bool** | If the OAI metadata prefix is the reference metadata | [optional] 
**res_id** | **str** | The _oai-metadata-prefixes_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**schema_namespace** | **str** | The XML schema namespace of the OAI metadata prefix | [optional] 
**schema_url** | **str** | The XML schema URL of the OAI metadata prefix | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


