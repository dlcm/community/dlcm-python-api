# AccessOrganizationalUnit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _organizational-units_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**description** | **str** | The description of the institution | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the organizational unit | [optional] 
**opening_date** | **date** | The opening date of the organizational unit | [optional] 
**res_id** | **str** | The _organizational-units_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**url** | **str** | The URL of the organizational unit | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


