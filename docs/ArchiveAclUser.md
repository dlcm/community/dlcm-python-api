# ArchiveAclUser

The user of the archive ACL
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**res_id** | **str** | The user ID of the archive ACL | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


