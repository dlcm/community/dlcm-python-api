# dlcmapi_client.DataMgmtApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**data_mgmt_get**](DataMgmtApi.md#data_mgmt_get) | **GET** /data-mgmt | 


# **data_mgmt_get**
> DataMgmt data_mgmt_get()



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.DataMgmtApi(api_client)
    
    try:
        api_response = api_instance.data_mgmt_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DataMgmtApi->data_mgmt_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DataMgmt**](DataMgmt.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

