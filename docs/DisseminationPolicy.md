# DisseminationPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _dissemination-policies_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**destination_server** | **str** | The destinationServer of the dissemination policy | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**name** | **str** | The name of the dissemination policy | [optional] 
**res_id** | **str** | The _dissemination-policies_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**sub_folder** | **str** | The subfolder of the dissemination policy | [optional] 
**type** | **str** | The type of the dissemination policy | [optional] 
**use_number** | **float** | Use number in organizational units of the dissemination policy | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


