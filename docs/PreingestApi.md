# dlcmapi_client.PreingestApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**preingest_contributors_get**](PreingestApi.md#preingest_contributors_get) | **GET** /preingest/contributors | 
[**preingest_contributors_get_by_id**](PreingestApi.md#preingest_contributors_get_by_id) | **GET** /preingest/contributors/{resId} | 
[**preingest_deposits_delete_by_id**](PreingestApi.md#preingest_deposits_delete_by_id) | **DELETE** /preingest/deposits/{resId} | 
[**preingest_deposits_get**](PreingestApi.md#preingest_deposits_get) | **GET** /preingest/deposits | 
[**preingest_deposits_get_by_id**](PreingestApi.md#preingest_deposits_get_by_id) | **GET** /preingest/deposits/{resId} | 
[**preingest_deposits_patch_by_id**](PreingestApi.md#preingest_deposits_patch_by_id) | **PATCH** /preingest/deposits/{resId} | 
[**preingest_deposits_post**](PreingestApi.md#preingest_deposits_post) | **POST** /preingest/deposits | 
[**preingest_get**](PreingestApi.md#preingest_get) | **GET** /preingest | 


# **preingest_contributors_get**
> Collection preingest_contributors_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.preingest_contributors_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_contributors_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_contributors_get_by_id**
> Contributor preingest_contributors_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.preingest_contributors_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_contributors_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Contributor**](Contributor.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_deposits_delete_by_id**
> Deposit preingest_deposits_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.preingest_deposits_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_deposits_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Deposit**](Deposit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_deposits_get**
> Collection preingest_deposits_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.preingest_deposits_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_deposits_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_deposits_get_by_id**
> Deposit preingest_deposits_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.preingest_deposits_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_deposits_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Deposit**](Deposit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_deposits_patch_by_id**
> Deposit preingest_deposits_patch_by_id(res_id, deposit=deposit)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
deposit = dlcmapi_client.Deposit() # Deposit |  (optional)

    try:
        api_response = api_instance.preingest_deposits_patch_by_id(res_id, deposit=deposit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_deposits_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **deposit** | [**Deposit**](Deposit.md)|  | [optional] 

### Return type

[**Deposit**](Deposit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_deposits_post**
> Deposit preingest_deposits_post(deposit=deposit)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    deposit = dlcmapi_client.Deposit() # Deposit |  (optional)

    try:
        api_response = api_instance.preingest_deposits_post(deposit=deposit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_deposits_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deposit** | [**Deposit**](Deposit.md)|  | [optional] 

### Return type

[**Deposit**](Deposit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preingest_get**
> Preingest preingest_get()



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.PreingestApi(api_client)
    
    try:
        api_response = api_instance.preingest_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PreingestApi->preingest_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Preingest**](Preingest.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

