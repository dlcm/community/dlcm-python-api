# SystemProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _system-properties_ resource | [optional] 
**checksums** | **list[str]** | The list of checksums used by the system | [optional] 
**default_checksum** | **str** | The default checksum type used by the system | [optional] 
**default_license** | **str** | The default license used by the system | [optional] 
**file_size_limit** | **str** | The maximum file size, in bytes, up to which data and virus verification are done | [optional] 
**forbidden_characters** | **list[str]** | The list of forbidden characters the system | [optional] 
**golden_formats** | **list[str]** | The list of golden formats of the system | [optional] 
**search_facets** | **list[str]** | Describes facet properties used on archives search page | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


