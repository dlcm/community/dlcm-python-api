# ArchiveAcl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _archive-acl_ resource | [optional] 
**aip_id** | **str** | The ID of the archive ACL | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**expiration** | **datetime** | The expiration date of the archive ACL | [optional] 
**expired** | **bool** | if the the archive ACL is expired | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**organizational_unit** | [**ArchiveAclOrganizationalUnit**](ArchiveAclOrganizationalUnit.md) |  | [optional] 
**res_id** | **str** | The _archive-acl_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**user** | [**ArchiveAclUser**](ArchiveAclUser.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


