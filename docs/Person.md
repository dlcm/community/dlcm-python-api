# Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**object**](.md) | The _links_ list of the _people_ resource | [optional] 
**creation** | [**ChangeInfo**](.md) |  | [optional] 
**first_name** | **str** | The first name of the person | [optional] 
**full_name** | **str** | The full name of the person | [optional] 
**last_name** | **str** | The last name of the person | [optional] 
**last_update** | [**ChangeInfo**](.md) |  | [optional] 
**orcid** | **str** | The ORCID of the person | [optional] 
**res_id** | **str** | The _people_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]] | [optional] 
**verified_orcid** | **bool** | if the person ORCID is verified | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


