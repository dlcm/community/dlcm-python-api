# AipInfoEmbargo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | **str** | Starting date of embargo period | [optional] 
**months** | **float** | Month number of embargo duration | [optional] 
**start_date** | **str** | Starting date of embargo period | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


