# dlcmapi_client.AccessApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**access_dip_delete_by_id**](AccessApi.md#access_dip_delete_by_id) | **DELETE** /access/dip/{resId} | 
[**access_dip_get**](AccessApi.md#access_dip_get) | **GET** /access/dip | 
[**access_dip_get_by_id**](AccessApi.md#access_dip_get_by_id) | **GET** /access/dip/{resId} | 
[**access_dip_patch_by_id**](AccessApi.md#access_dip_patch_by_id) | **PATCH** /access/dip/{resId} | 
[**access_dip_post**](AccessApi.md#access_dip_post) | **POST** /access/dip | 
[**access_get**](AccessApi.md#access_get) | **GET** /access | 
[**access_metadata_get**](AccessApi.md#access_metadata_get) | **GET** /access/metadata | 
[**access_metadata_get_by_id**](AccessApi.md#access_metadata_get_by_id) | **GET** /access/metadata/{resId} | 
[**access_orders_delete_by_id**](AccessApi.md#access_orders_delete_by_id) | **DELETE** /access/orders/{resId} | 
[**access_orders_get**](AccessApi.md#access_orders_get) | **GET** /access/orders | 
[**access_orders_get_by_id**](AccessApi.md#access_orders_get_by_id) | **GET** /access/orders/{resId} | 
[**access_orders_patch_by_id**](AccessApi.md#access_orders_patch_by_id) | **PATCH** /access/orders/{resId} | 
[**access_orders_post**](AccessApi.md#access_orders_post) | **POST** /access/orders | 
[**access_organizational_units_get**](AccessApi.md#access_organizational_units_get) | **GET** /access/organizational-units | 
[**access_organizational_units_get_by_id**](AccessApi.md#access_organizational_units_get_by_id) | **GET** /access/organizational-units/{resId} | 


# **access_dip_delete_by_id**
> Dip access_dip_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.access_dip_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_dip_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Dip**](Dip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_dip_get**
> Collection access_dip_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.access_dip_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_dip_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_dip_get_by_id**
> Dip access_dip_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.access_dip_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_dip_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Dip**](Dip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_dip_patch_by_id**
> Dip access_dip_patch_by_id(res_id, dip=dip)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
dip = dlcmapi_client.Dip() # Dip |  (optional)

    try:
        api_response = api_instance.access_dip_patch_by_id(res_id, dip=dip)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_dip_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **dip** | [**Dip**](Dip.md)|  | [optional] 

### Return type

[**Dip**](Dip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_dip_post**
> Dip access_dip_post(dip=dip)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    dip = dlcmapi_client.Dip() # Dip |  (optional)

    try:
        api_response = api_instance.access_dip_post(dip=dip)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_dip_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dip** | [**Dip**](Dip.md)|  | [optional] 

### Return type

[**Dip**](Dip.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_get**
> Access access_get()



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    
    try:
        api_response = api_instance.access_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Access**](Access.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_metadata_get**
> Collection access_metadata_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.access_metadata_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_metadata_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_metadata_get_by_id**
> Metadata access_metadata_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.access_metadata_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_metadata_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Metadata**](Metadata.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_orders_delete_by_id**
> Order access_orders_delete_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.access_orders_delete_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_orders_delete_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Order**](Order.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_orders_get**
> Collection access_orders_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.access_orders_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_orders_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_orders_get_by_id**
> Order access_orders_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.access_orders_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_orders_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**Order**](Order.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_orders_patch_by_id**
> Order access_orders_patch_by_id(res_id, order=order)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)
order = dlcmapi_client.Order() # Order |  (optional)

    try:
        api_response = api_instance.access_orders_patch_by_id(res_id, order=order)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_orders_patch_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 
 **order** | [**Order**](Order.md)|  | [optional] 

### Return type

[**Order**](Order.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_orders_post**
> Order access_orders_post(order=order)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    order = dlcmapi_client.Order() # Order |  (optional)

    try:
        api_response = api_instance.access_orders_post(order=order)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_orders_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**Order**](Order.md)|  | [optional] 

### Return type

[**Order**](Order.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | 201 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_organizational_units_get**
> Collection access_organizational_units_get(size=size, page=page, sort=sort)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    size = 56 # int | The page size (optional)
page = 56 # int | The current page number (optional)
sort = 'sort_example' # str | To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order. (optional)

    try:
        api_response = api_instance.access_organizational_units_get(size=size, page=page, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_organizational_units_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| The page size | [optional] 
 **page** | **int**| The current page number | [optional] 
 **sort** | **str**| To sort on a field. By default, the sort is ascending. desc option &#39;[,desc]&#39; permits to have descending order. | [optional] 

### Return type

[**Collection**](Collection.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **access_organizational_units_get_by_id**
> AccessOrganizationalUnit access_organizational_units_get_by_id(res_id)



### Example

* OAuth Authentication (dlcm-auth):
```python
from __future__ import print_function
import time
import dlcmapi_client
from dlcmapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: dlcm-auth
configuration = dlcmapi_client.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with dlcmapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dlcmapi_client.AccessApi(api_client)
    res_id = 'res_id_example' # str | Resource Id (GUID)

    try:
        api_response = api_instance.access_organizational_units_get_by_id(res_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessApi->access_organizational_units_get_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **res_id** | **str**| Resource Id (GUID) | 

### Return type

[**AccessOrganizationalUnit**](AccessOrganizationalUnit.md)

### Authorization

[dlcm-auth](../README.md#dlcm-auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

