# DLCM APIs in Python

## How to generate APIs
*  Use OpenAPI Generator : https://openapi-generator.tech/
*  Validate the JSON file `openapi-generator validate -i dlcm-api.json`
*  Run the command `openapi-generator generate -i dlcm-api.json -g python -o ./ --package-name dlcmapi_client --additional-properties="packageVersion=x.y.z,projectName=dlcm-api,infoName=University of Geneva,infoEmail=eresearch-opensource@unige.ch"`
* At the root of this repo, run the command `cat dlcmapi_client/api/preingest.py _deposits_contributor_addendum.py > dlcmapi_client/api/preingest.py`
