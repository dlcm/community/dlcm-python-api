# coding: utf-8

"""
    DLCM-Solution

    DLCM application wrapping all modules, All-In-One  # noqa: E501

    The version of the OpenAPI document: 2.1.1
    Contact: eresearch-opensource@unige.ch
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import dlcmapi_client
from dlcmapi_client.api.preingest_api import PreingestApi  # noqa: E501
from dlcmapi_client.rest import ApiException


class TestPreingestApi(unittest.TestCase):
    """PreingestApi unit test stubs"""

    def setUp(self):
        self.api = dlcmapi_client.api.preingest_api.PreingestApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_preingest_contributors_get(self):
        """Test case for preingest_contributors_get

        """
        pass

    def test_preingest_contributors_get_by_id(self):
        """Test case for preingest_contributors_get_by_id

        """
        pass

    def test_preingest_deposits_delete_by_id(self):
        """Test case for preingest_deposits_delete_by_id

        """
        pass

    def test_preingest_deposits_get(self):
        """Test case for preingest_deposits_get

        """
        pass

    def test_preingest_deposits_get_by_id(self):
        """Test case for preingest_deposits_get_by_id

        """
        pass

    def test_preingest_deposits_patch_by_id(self):
        """Test case for preingest_deposits_patch_by_id

        """
        pass

    def test_preingest_deposits_post(self):
        """Test case for preingest_deposits_post

        """
        pass

    def test_preingest_get(self):
        """Test case for preingest_get

        """
        pass


if __name__ == '__main__':
    unittest.main()
