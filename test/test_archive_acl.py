# coding: utf-8

"""
    DLCM-Solution

    DLCM application wrapping all modules, All-In-One  # noqa: E501

    The version of the OpenAPI document: 2.1.1
    Contact: eresearch-opensource@unige.ch
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import dlcmapi_client
from dlcmapi_client.models.archive_acl import ArchiveAcl  # noqa: E501
from dlcmapi_client.rest import ApiException

class TestArchiveAcl(unittest.TestCase):
    """ArchiveAcl unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test ArchiveAcl
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = dlcmapi_client.models.archive_acl.ArchiveAcl()  # noqa: E501
        if include_optional :
            return ArchiveAcl(
                links = None, 
                aip_id = '0', 
                creation = dlcmapi_client.models.change_info.change-info(
                    when = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                    who = '0', ), 
                expiration = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                expired = True, 
                last_update = dlcmapi_client.models.change_info.change-info(
                    when = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                    who = '0', ), 
                organizational_unit = dlcmapi_client.models.archive_acl_organizational_unit.archive_acl_organizationalUnit(
                    res_id = '0', ), 
                res_id = '0', 
                user = dlcmapi_client.models.archive_acl_user.archive_acl_user(
                    res_id = '0', )
            )
        else :
            return ArchiveAcl(
        )

    def testArchiveAcl(self):
        """Test ArchiveAcl"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
