# coding: utf-8

"""
    DLCM-Solution

    DLCM application wrapping all modules, All-In-One  # noqa: E501

    The version of the OpenAPI document: 2.1.1
    Contact: eresearch-opensource@unige.ch
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import dlcmapi_client
from dlcmapi_client.models.notification import Notification  # noqa: E501
from dlcmapi_client.rest import ApiException

class TestNotification(unittest.TestCase):
    """Notification unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test Notification
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = dlcmapi_client.models.notification.Notification()  # noqa: E501
        if include_optional :
            return Notification(
                links = None, 
                creation = dlcmapi_client.models.change_info.change-info(
                    when = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                    who = '0', ), 
                emitter = None, 
                last_update = dlcmapi_client.models.change_info.change-info(
                    when = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                    who = '0', ), 
                message = '0', 
                notification_status = 'PROCESSED', 
                notification_type = dlcmapi_client.models.notification_notification_type.notification_notificationType(
                    res_id = '0', ), 
                notified_org_unit = dlcmapi_client.models.notification_notified_org_unit.notification_notifiedOrgUnit(
                    res_id = '0', ), 
                object_id = '0', 
                recipient = dlcmapi_client.models.notification_recipient.notification_recipient(
                    res_id = '0', ), 
                res_id = '0', 
                status_message = '0'
            )
        else :
            return Notification(
        )

    def testNotification(self):
        """Test Notification"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
